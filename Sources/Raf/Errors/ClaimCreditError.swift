//
//  ClaimCreditError.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 01/08/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public enum ClaimCreditError: Error {
    case requiresRelogin(debugMessage: String)
    case generalError(debugMessage: String)
    case connectivityError(debugMessage: String)
}

public class ClaimCreditErrorResolver: ErrorResolver {
    public typealias T = ClaimCreditError
    
    public func fromRequiresReloginError(debugMessage: String) -> ClaimCreditError {
        return .requiresRelogin(debugMessage: debugMessage)
    }
    
    public func fromInvalidParametersError(debugMessage: String) -> ClaimCreditError {
        return .generalError(debugMessage: "Unexpected invalid parameters \(debugMessage)")
    }
    
    public func fromGeneralError(debugMessage: String) -> ClaimCreditError {
        return .generalError(debugMessage: debugMessage)
    }
    
    public func fromConnectivityError(debugMessage: String) -> ClaimCreditError {
        return .connectivityError(debugMessage:debugMessage)
    }
    
    public func fromTypeSpecificError(_ statusCode: Int, _ reason: String, _ message: String) -> ClaimCreditError? {
        return nil
    }
}



extension ClaimCreditError: CopilotLocalizedError {
    public func errorPrefix() -> String {
        return "Claim Credit"
    }
    
    public var errorDescription: String? {
        switch self {
        case .requiresRelogin(let debugMessage):
            return requiresReloginMessage(debugMessage: debugMessage)
        case .generalError(let debugMessage):
            return generalErrorMessage(debugMessage: debugMessage)
        case .connectivityError(let debugMessage):
            return connectivityErrorMessage(debugMessage: debugMessage)
        }
    }
}
