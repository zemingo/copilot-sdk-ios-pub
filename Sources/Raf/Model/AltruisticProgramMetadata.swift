//
//  AltruisticProgramMetadata.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 27/08/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public class AltruisticProgramMetadata {
    
    private struct Keys {
        static let descriptionKey = "description"
        static let shareTextKey = "shareText"
    }
    
    public let description: String
    public let shareText: String
    
    // MARK: - Init
    
    init?(withDictionary dictionary: [String: Any]) {
        guard let description = dictionary[Keys.descriptionKey] as? String,
            let shareText = dictionary[Keys.shareTextKey] as? String else {
                return nil
        }
        
        self.description = description
        self.shareText = shareText
    }
}
