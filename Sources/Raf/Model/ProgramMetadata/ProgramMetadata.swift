//
//  ProgramMetadata.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 27/08/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public class ProgramMetadata {
    
    private struct Keys {
        static let discountKey = "discount"
        static let mainProductNameKey = "mainProductName"
        static let rewardKey = "reward"
        static let shareTextKey = "shareText"
        static let topBannerKey = "topBanner"
    }
    
    public let discount: ProgramTerm?
    public let mainProductName: String
    public let reward: ProgramTerm
    public let shareText: String?
    public let topBanner: String
    
    // MARK: - Init
    
    init?(withDictionary dictionary: [String: Any]) {
        guard let mainProductName = dictionary[Keys.mainProductNameKey] as? String,
            let rewardDict = dictionary[Keys.rewardKey] as? [String:Any],
            let reward = ProgramTerm(withDictionary: rewardDict),
            let topBanner = dictionary[Keys.topBannerKey] as? String else {
                return nil
        }
        
        var discount: ProgramTerm? = nil
        if let discountDict = dictionary[Keys.discountKey] as? [String:Any] {
            discount = ProgramTerm(withDictionary: discountDict)
        }
        
        self.discount = discount
        self.mainProductName = mainProductName
        self.reward = reward
        self.shareText = dictionary[Keys.shareTextKey] as? String
        self.topBanner = topBanner
    }
}
