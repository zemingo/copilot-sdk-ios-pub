//
//  RafServiceInteraction.swift
//  ZemingoBLELayer
//
//  Created by Revital Pisman on 19/06/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import CopilotLogger

protocol RafServiceInteractable {
    func getRafData(getRafDataClosure: @escaping FetchRafDataClosure)
    func generateReferralCoupon(generateReferralCouponClosure: @escaping GenerateReferralCouponClosure)
    func getJobStatus(jobId: String, getJobStatusClosure: @escaping GetJobStatusClosure)
    func claimCredit(rewardsIds: [String], claimCreditClosure: @escaping ClaimCreditClosure)
}

internal class RafServiceInteraction {
    
    fileprivate var authenticatedRequestExecquter: AuthenticatedRequestExecutor<RafService>

    // MARK: - Init
    
    init(authenticationProvider: AuthenticationProvider) {
        authenticatedRequestExecquter = AuthenticatedRequestExecutor<RafService>(authenticationProvider: authenticationProvider)
    }
    
    //MARK: - Private
    
    fileprivate func rafDataMapper(_ rafDataResponse: RafDataResponse) -> RafData {
        let staticData = StaticData(footerTitle: rafDataResponse.generalMetadata.footerTitle, storeName: rafDataResponse.generalMetadata.storeName, termsOfUseUrl: rafDataResponse.generalMetadata.termsAndConditions)
        
        var rafProgram: RafProgram? = nil
        if let programMetadata = rafDataResponse.programMetadata {
            rafProgram = RafProgram(bannerText: programMetadata.topBanner, rewardCurrencySymbol: programMetadata.reward.currencySymbol, rewardValue: programMetadata.reward.value, shareText: programMetadata.shareText)
        }
        
        let altruisticProgram = AltruisticProgram(description: rafDataResponse.altruisticProgramMetadata.description, shareText: rafDataResponse.altruisticProgramMetadata.shareText)
        
        var rewards = [Reward]()
        for reward in rafDataResponse.rewards {
            let rewardStatus: RewardStatus
            switch reward.status {
            case .available:
                rewardStatus = .active
            case .pending:
                rewardStatus = .pending
            case .claimed:
                rewardStatus = .claimed
            case .canceled:
                rewardStatus = .returned
            }
            rewards.append(Reward(id: reward.id, status: rewardStatus, value: reward.value, currency: reward.currencySymbol))
        }
        
        var discountCodes = [DiscountCode]()
        for discountCode in rafDataResponse.rewardsCoupons {
            discountCodes.append(DiscountCode(value: discountCode.value, currencySymbol: discountCode.currencySymbol, code: discountCode.code))
        }
        
        var dynamicData: DynamicData? = nil
        //String "1" is the only supported templateId for dynamicInfo for now
        if rafDataResponse.generalMetadata.dynamicInfo?.templateId == "1" {
            if let dynamicInfo = rafDataResponse.generalMetadata.dynamicInfo {
                var elements = [DynamicItem]()
                for element in dynamicInfo.dynamicContent.elements {
                    elements.append(DynamicItem(title: element.title, description: element.description, imageUrl: element.imageUrl, actionUrl: element.actionUrl, auxiliaryText: element.auxiliaryText))
                }
                
                dynamicData = DynamicData(title: dynamicInfo.dynamicContent.title, dynamicItems: elements)
            }
        }
        
        var pendingJobs = [RafJob]()
        if let pendingJobsResponse = rafDataResponse.pendingJobs {
            for rafJobResponse in pendingJobsResponse {
                let rafJobStatus: RafJobStatus
                switch rafJobResponse.status {
                case .inProgress:
                    rafJobStatus = .inProgress
                case .success:
                    rafJobStatus = .success
                case .failure:
                    rafJobStatus = .failure
                }
                
                let rafJobType: RafJobType
                switch rafJobResponse.type {
                case .generateCoupon:
                    rafJobType = .generateCoupon
                case .generateReward:
                    rafJobType = .generateReward
                }
                pendingJobs.append(RafJob(id: rafJobResponse.id, status: rafJobStatus, type: rafJobType, retryAfter: nil))
            }
        }
        
        return RafData(staticData: staticData, rafProgram: rafProgram, altruisticProgram: altruisticProgram, rewards: rewards, discountCodes: discountCodes, dynamicData: dynamicData, pendingJobs: pendingJobs)
    }
    
    fileprivate func rafJobMapper(_ rafJobResponse: RafJobResponse, retryAfter: Int? = nil) -> RafJob {
        let jobStatus: RafJobStatus
        switch rafJobResponse.status {
        case .inProgress:
            jobStatus = .inProgress
        case .success:
            jobStatus = .success
        case .failure:
            jobStatus = .failure
        }
        
        let jobType: RafJobType
        switch rafJobResponse.type {
        case .generateCoupon:
            jobType = .generateCoupon
        case .generateReward:
            jobType = .generateReward
        }
        
        return RafJob(id: rafJobResponse.id, status: jobStatus, type: jobType, retryAfter: retryAfter)
    }
    
    fileprivate func handleGetRafDataExecuterResult(_ response: RequestExecutorResponse, getRafDataClosure: FetchRafDataClosure) {
        var getRafDataResponse: Response<RafData, FetchRafDataError>
        
        switch response {
        case .failure(error: let serverError):
            getRafDataResponse = .failure(error: FetchRafDataErrorResolver().resolve(serverError))
            
        case .success(payload: let dictionary):
            if let rafDataRsponse = RafDataResponse(withDictionary: dictionary) {
                
                let rafData = rafDataMapper(rafDataRsponse)
                
                getRafDataResponse = .success(rafData)
            }
            else {
                getRafDataResponse = .failure(error: .generalError(debugMessage: "Failed to create user from dictionary"))
            }
        }
        
        getRafDataClosure(getRafDataResponse)
    }
    
    fileprivate func handleGenerateReferralCouponExecuterResult(_ response: RequestExecutorResponse, generateReferralCouponClosure: GenerateReferralCouponClosure) {
        var getCurrnetRafDataResponse: Response<RafJob, GenerateReferralCouponError>
        
        switch response {
        case .failure(error: let serverError):
            getCurrnetRafDataResponse = .failure(error: GenerateReferralCouponErrorResolver().resolve(serverError))
            
        case .success(payload: let dictionary):
            if let rafJobResponse = RafJobResponse(withDictionary: dictionary) {
                let rafJob = rafJobMapper(rafJobResponse)
                getCurrnetRafDataResponse = .success(rafJob)
            }
            else {
                getCurrnetRafDataResponse = .failure(error: .generalError(debugMessage: "Failed to create user from dictionary"))
            }
        }
        
        generateReferralCouponClosure(getCurrnetRafDataResponse)
    }
    
    fileprivate func handleGetJobStatusExecuterResult(_ response: RequestExecutorWithHeadersResponse, getJobStatusClosure: GetJobStatusClosure) {
        var getJobStatusResponse: Response<JobStatus, GetJobStatusError>
        
        switch response {
        case .failure(error: let serverError):
            getJobStatusResponse = .failure(error: GetJobStatusErrorResolver().resolve(serverError))
            
        case .success(payload: let httpResponse):
            if let jobStatusResponse = JobStatusResponse(withDictionary: httpResponse.body) {
                let retryAfter = httpResponse.headers?["Retry-After"] as? String ?? ""
                let retryAfterNum: Int = Int(retryAfter) ?? 1
                let rafJob = rafJobMapper(jobStatusResponse.job, retryAfter: retryAfterNum)
                
                var rafData: RafData? = nil
                if let rafDataResponse = jobStatusResponse.rafDataResponse {
                    rafData = rafDataMapper(rafDataResponse)
                }
                
                let jobStatus = JobStatus(rafJob: rafJob, rafData: rafData)
                getJobStatusResponse = .success(jobStatus)
            }
            else {
                getJobStatusResponse = .failure(error: .generalError(debugMessage: "Failed to create user from dictionary"))
            }
        }
        
        getJobStatusClosure(getJobStatusResponse)
    }
    
    fileprivate func handleClaimCreditExecuterResult(_ response: RequestExecutorResponse, claimCreditClosure: ClaimCreditClosure) {
        var claimCreditResponse: Response<RafJob, ClaimCreditError>
        
        switch response {
        case .failure(error: let serverError):
            claimCreditResponse = .failure(error: ClaimCreditErrorResolver().resolve(serverError))
            
        case .success(payload: let dictionary):
            if let rafJobResponse = RafJobResponse(withDictionary: dictionary) {
                let rafJob = rafJobMapper(rafJobResponse)
                claimCreditResponse = .success(rafJob)
            }
            else {
                claimCreditResponse = .failure(error: .generalError(debugMessage: "Failed to create user from dictionary"))
            }
        }
        
        claimCreditClosure(claimCreditResponse)
    }
}

extension RafServiceInteraction: RafServiceInteractable {
    
    internal func getRafData(getRafDataClosure: @escaping FetchRafDataClosure) {
        let getRafDataService = RafService.getRafData
        
        authenticatedRequestExecquter.executeRequest(target: getRafDataService) { [weak self] (requestExecuterResponse) in
            if let weakSelf = self {
                weakSelf.handleGetRafDataExecuterResult(requestExecuterResponse, getRafDataClosure: getRafDataClosure)
            }
            else {
                ZLogManagerWrapper.sharedInstance.logError(message: "self is nil")
            }
        }
    }
    
    func generateReferralCoupon(generateReferralCouponClosure: @escaping GenerateReferralCouponClosure) {
        let generateReferralCouponService = RafService.generateReferralCoupon
        
        authenticatedRequestExecquter.executeRequest(target: generateReferralCouponService) { [weak self] (requestExecuterResponse) in
            if let weakSelf = self {
                weakSelf.handleGenerateReferralCouponExecuterResult(requestExecuterResponse, generateReferralCouponClosure: generateReferralCouponClosure)
            }
            else {
                ZLogManagerWrapper.sharedInstance.logError(message: "self is nil")
            }
        }
    }
    
    func getJobStatus(jobId: String, getJobStatusClosure: @escaping GetJobStatusClosure) {
        let getJobStatusService = RafService.getJobStatus(jobId: jobId)
        
        authenticatedRequestExecquter.executeRequestWithHeaders(target: getJobStatusService) { [weak self] (requestExecuterResponse) in
            if let weakSelf = self {
                weakSelf.handleGetJobStatusExecuterResult(requestExecuterResponse, getJobStatusClosure: getJobStatusClosure)
            }
            else {
                ZLogManagerWrapper.sharedInstance.logError(message: "self is nil")
            }
        }
    }
    
    func claimCredit(rewardsIds: [String], claimCreditClosure: @escaping ClaimCreditClosure) {
        let claimCreditService = RafService.claimCredit(rewardsIds: rewardsIds)
        
        authenticatedRequestExecquter.executeRequest(target: claimCreditService) { [weak self] (requestExecuterResponse) in
            if let weakSelf = self {
                weakSelf.handleClaimCreditExecuterResult(requestExecuterResponse, claimCreditClosure: claimCreditClosure)
            }
            else {
                ZLogManagerWrapper.sharedInstance.logError(message: "self is nil")
            }
        }
    }
    
}
