//
//  RafServiceInteractionClosures.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 30/07/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

typealias FetchRafDataClosure = (Response<RafData, FetchRafDataError>) -> Void
typealias GenerateReferralCouponClosure = (Response<RafJob, GenerateReferralCouponError>) -> Void
typealias GetJobStatusClosure = (Response<JobStatus, GetJobStatusError>) -> Void
typealias ClaimCreditClosure = (Response<RafJob, ClaimCreditError>) -> Void

