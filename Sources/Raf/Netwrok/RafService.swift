//
//  RafService.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 29/07/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import Moya

internal enum RafService {
    case getRafData
    case generateReferralCoupon
    case getJobStatus(jobId: String)
    case claimCredit(rewardsIds: [String])
}

extension RafService: TargetType, AccessTokenAuthorizable {
    
    private struct Consts {
        static let systemPath = "\(NetworkParameters.shared.apiVersion)/raf/"
        
        static let rafDataPathSuffix = "me/"
        static let referrersPathSuffix = "referrers/"
        static let jobsPathSuffix = "jobs/"
        
        static let generateReferralCouponPathSuffix = Consts.rafDataPathSuffix + "coupons/"
        static let rewardCouponDataPathSuffix = Consts.rafDataPathSuffix + "rewards/"
        static let claimCreditPathSuffix = rewardCouponDataPathSuffix + "claim/"
    }
    
    public var baseURL: URL {
        return NetworkParameters.shared.baseURL
    }
    
    var authorizationType: AuthorizationType {
        return .bearer
    }
    
    public var path: String {
        switch self {
        case .getRafData:
            return Consts.systemPath + Consts.referrersPathSuffix + Consts.rafDataPathSuffix
        case .generateReferralCoupon:
            return Consts.systemPath + Consts.referrersPathSuffix + Consts.generateReferralCouponPathSuffix
        case .getJobStatus(jobId: let jobId):
            return Consts.systemPath + Consts.jobsPathSuffix + jobId
        case .claimCredit:
            return Consts.systemPath + Consts.referrersPathSuffix + Consts.claimCreditPathSuffix
        }
    }
    
    public var method: Moya.Method  {
        switch self {
        case .getRafData:
            fallthrough
        case .getJobStatus(jobId: _):
            return .get
        case .generateReferralCoupon:
            fallthrough
        case .claimCredit:
            return .post
        }
    }
    
    public var parameters: [String: Any]? {
        switch self {
        case .getRafData:
            return nil
        case .generateReferralCoupon:
            return nil
        case .getJobStatus(jobId: _):
            return nil
        case .claimCredit(rewardsIds: let rewardsIds):
            return ["rewards" : rewardsIds]
        }
    }
    
    public var parameterEncoding: ParameterEncoding {
        switch self {
        case .getRafData:
            fallthrough
        case .generateReferralCoupon:
            return URLEncoding.default
        case .getJobStatus:
            fallthrough
        case .claimCredit:
            return JSONEncoding.default
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        if let params = parameters {
            return .requestParameters(parameters: params, encoding: parameterEncoding)
        } else {
            return .requestPlain
        }
    }
    
    public var headers: [String : String]? {
        return nil
    }
}

