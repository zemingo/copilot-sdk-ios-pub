//
//  JobsRecoveryWorker.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 05/09/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import CopilotLogger

public typealias JobsRecoveryWorkerClosure = (Bool) -> Void

public class JobsRecoveryWorker: StoppableWorker {
    
    private let pendingRafJobs: [RafJob]
    private var stoppableWorkers = [StoppableWorker]()
    private let closure: JobsRecoveryWorkerClosure
    
    private let queue = DispatchQueue.global(qos: .background)
    private let dispatchGroup = DispatchGroup()
    private var timeoutTask: DispatchWorkItem?
    
    private let recoverTimeoutSeconds: Double = 30
    private var wasTimedOut = false
    
    typealias Dependencies = HasRafServiceInteraction
    private let dependencies: Dependencies

    init(dependencies: Dependencies, pendingRafJobs: [RafJob], closure: @escaping JobsRecoveryWorkerClosure) {
        self.dependencies = dependencies
        self.pendingRafJobs = pendingRafJobs
        self.closure = closure
    }
    
    public func startExecution() -> JobsRecoveryWorker {
        ZLogManagerWrapper.sharedInstance.logInfo(message: "Starting to recover \(pendingRafJobs.count) pending jobs")
        
        timeoutTask = DispatchWorkItem { [weak self] in
            self?.wasTimedOut = true
            self?.stopExecution()
        }
        if let timeoutTask = timeoutTask {
            queue.asyncAfter(deadline: .now() + recoverTimeoutSeconds, execute: timeoutTask)
        }
        
        for pendingRafJob in pendingRafJobs {
            dispatchGroup.enter()
            switch pendingRafJob.type {
            case .generateCoupon:
                recoverGenerateCouponJob(pendingRafJob)
            case .generateReward:
                recoverClaimRewardJob(pendingRafJob)
            }
        }
        
        dispatchGroup.notify(queue: queue) { [weak self] in
            if let wasTimedOut = self?.wasTimedOut, wasTimedOut {
                ZLogManagerWrapper.sharedInstance.logInfo(message: "Jobs recovery hit timeout")
            } else {
                ZLogManagerWrapper.sharedInstance.logInfo(message: "Recovery finished")
            }
            self?.closure(true)
        }
        
        return self
    }
    
    func stopExecution() {
        ZLogManagerWrapper.sharedInstance.logInfo(message: "Stopping recovery")
        if let timeoutTask = self.timeoutTask, !timeoutTask.isCancelled {
            timeoutTask.cancel()
            closure(false)
        }
        for stoppableWorker in stoppableWorkers {
            stoppableWorker.stopExecution()
        }
    }
    
    private func recoverGenerateCouponJob(_ pendingGenerateCouponJob: RafJob) {
        ZLogManagerWrapper.sharedInstance.logInfo(message: "Recovering \(pendingGenerateCouponJob.type.rawValue) job with id \(pendingGenerateCouponJob.id)")
        let generateCouponWorker = RafPollingWorker<GenerateReferralCouponError>(dependencies: dependencies, jobId: pendingGenerateCouponJob.id) { [weak self] (response) in
            
            switch response {
            case .success(_):
                ZLogManagerWrapper.sharedInstance.logInfo(message: "Succeed to recover \(pendingGenerateCouponJob.type.rawValue) job with id \(pendingGenerateCouponJob.id)")
                self?.dispatchGroup.leave()
                
            case .failure(error: let error):
                ZLogManagerWrapper.sharedInstance.logInfo(message: "Error recovering \(pendingGenerateCouponJob.type.rawValue) job with id \(pendingGenerateCouponJob.id) with error \(error)")
                self?.dispatchGroup.leave()
            }
            
            }.startExecution()
        stoppableWorkers.append(generateCouponWorker)
    }
    
    private func recoverClaimRewardJob(_ pendingClaimRewardJob: RafJob) {
        ZLogManagerWrapper.sharedInstance.logInfo(message: "Recovering \(pendingClaimRewardJob.type.rawValue) job with id \(pendingClaimRewardJob.id)")
        let claimCreditWorker = RafPollingWorker<ClaimCreditError>(dependencies: dependencies, jobId: pendingClaimRewardJob.id) { [weak self] (response) in
            
            switch response {
            case .success(_):
                ZLogManagerWrapper.sharedInstance.logInfo(message: "Succeed to recover \(pendingClaimRewardJob.type.rawValue) job with id \(pendingClaimRewardJob.id)")
                self?.dispatchGroup.leave()
                
            case .failure(error: let error):
                ZLogManagerWrapper.sharedInstance.logInfo(message: "Error recovering \(pendingClaimRewardJob.type.rawValue) job with id \(pendingClaimRewardJob.id) with error \(error)")
                self?.dispatchGroup.leave()
            }
            
            }.startExecution()
        stoppableWorkers.append(claimCreditWorker)
    }
}
