//
//  JobCreator.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 02/09/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

typealias JobCreatorClosure<JobCreatorError: Error> = (Response<RafJob, JobCreatorError>) -> Void

class JobCreator<JobCreatorError: Error> {
    
    func createJob(_ closure: @escaping JobCreatorClosure<JobCreatorError>) {
        fatalError("For Subclassing")
    }
    
    func getGeneralError(withDebugMessage debugMessage: String) -> JobCreatorError {
        fatalError("For Subclassing")
    }
    
    func mapPollingError(withGetJobStatusError getJobStatusError: GetJobStatusError) -> JobCreatorError {
        fatalError("For Subclassing")
    }
}
