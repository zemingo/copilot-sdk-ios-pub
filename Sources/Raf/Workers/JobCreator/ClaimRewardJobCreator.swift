//
//  ClaimRewardJobCreator.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 02/09/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

class ClaimRewardJobCreator: JobCreator<ClaimCreditError> {
    
    typealias Dependencies = HasRafServiceInteraction
    private let dependencies: Dependencies
    
    private let rewardsIds: [String]
    
    init(rewardsIds: [String], dependencies: Dependencies) {
        self.dependencies = dependencies
        self.rewardsIds = rewardsIds
    }
    
    override func createJob(_ closure: @escaping JobCreatorClosure<ClaimCreditError>) {
        dependencies.rafServiceInteraction.claimCredit(rewardsIds: rewardsIds, claimCreditClosure: closure)
    }
    
    override func getGeneralError(withDebugMessage debugMessage: String) -> ClaimCreditError {
        return ClaimCreditError.generalError(debugMessage: debugMessage)
    }
    
    override func mapPollingError(withGetJobStatusError getJobStatusError: GetJobStatusError) -> ClaimCreditError {
        let error: ClaimCreditError
        
        switch getJobStatusError {
        case .requiresRelogin(let debugMessage):
            error = ClaimCreditError.requiresRelogin(debugMessage: debugMessage)
        case .generalError(let debugMessage):
            error = ClaimCreditError.generalError(debugMessage: debugMessage)
        case .connectivityError(let debugMessage):
            error = ClaimCreditError.connectivityError(debugMessage: debugMessage)
        }
        
        return error
    }
    
}
