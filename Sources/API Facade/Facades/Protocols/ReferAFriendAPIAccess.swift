//
//  ReferAFriendAPIAccess.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 25/06/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public protocol ReferAFriendAPIAccess: class {
    
    func getRafViewController() -> ReferAFriendViewController?
    
    func fetchRafData() -> RequestBuilder<RafData, FetchRafDataError>
    
    func getGenerateCouponWorker(_ closure: @escaping RafPollingWorkerClosure<GenerateReferralCouponError>) -> RafPollingWorker<GenerateReferralCouponError>
    
    func getClaimRewardWorker(withRewardsIds rewardsIds: [String], _ closure: @escaping RafPollingWorkerClosure<ClaimCreditError>) -> RafPollingWorker<ClaimCreditError>
    
    func getPendingJobsRecoveryWorker(withPendingRafJobs pendingRafJobs: [RafJob], _ closure: @escaping JobsRecoveryWorkerClosure) -> JobsRecoveryWorker
}
