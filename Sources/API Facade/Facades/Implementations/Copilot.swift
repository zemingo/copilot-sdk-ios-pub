//
//  Copilot.swift
//  CopilotAPIAccess
//
//  Created by Richard Houta on 08/10/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import Foundation
import CopilotLogger

public class Copilot {
    
    public static let instance: Copilot = Copilot()
    public let report: ReportAPIAccess
    public let manage: Manage
    public let referAFriend: ReferAFriendAPIAccess
    public let inAppMessages: InAppManagerAPIAccess
    

    private let sessionNotifier = SessionNotifier()
    
    private init() {

        let bundleConfigurationProvider: ConfigurationProvider = BundleConfigurationProvider()
        CopilotConfigurationValidator(copilotBundleConfigurationProvider: bundleConfigurationProvider).validate()
        
        NetworkParameters.shared.setConfigurationProvider(bundleConfigurationProvider)
        
        let authenticationProviderContainer = AuthenticationProviderContainer()
        
        report = ReportAPI(isGDPRCompliant: bundleConfigurationProvider.isGdprCompliant ?? false)

        manage = Manage(authenticationProviderContainer: authenticationProviderContainer, reporter: report, configurationProvider: bundleConfigurationProvider,sessionObserver: sessionNotifier)
        
        KeyboardStateListener.shared.start()
 
        let authenticationProvider: AuthenticationProvider
        if let authProvider = authenticationProviderContainer.authenticationProvider {
            authenticationProvider = authProvider
        } else {
            authenticationProvider = EmptyAuthenticationProvider()
            ZLogManagerWrapper.sharedInstance.logError(message: "Failed to create Copilot/External Authentication Provider")
        }
        
        let copilotDependencies = CopilotDependencies(authenticationProvider: authenticationProvider,
                                           rafServiceInteraction: RafServiceInteraction(authenticationProvider: authenticationProvider))
        
        referAFriend = ReferAFriendAPI(dependencies: copilotDependencies)
        let iamManager = InAppManager(reportApiAccess: report, authenticationProvider: authenticationProvider)
        inAppMessages = iamManager
        sessionNotifier.registerObserver(observer: iamManager)
    }
    
    // MARK: - Public Setup
    
    /// Setup Copilot's analytics dependencies with the given providers. This needs to be called after the app is launched, prior to using Copilot services.
    public static func setup(analyticsProviders: [EventLogProvider]) {
        analyticsProviders.forEach {
            AnalyticsEventsManager.sharedInstance.eventsProviderHandler.addEventLogProvider(newEventLogProvider: $0)
        }
    }
}
