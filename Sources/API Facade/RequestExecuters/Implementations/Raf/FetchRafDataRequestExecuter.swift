//
//  FetchRafDataRequestExecuter.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 30/07/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

class FetchRafDataRequestExecuter: RequestExecuter<RafData, FetchRafDataError> {
    
    typealias Dependencies = HasRafServiceInteraction
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
        super.init()
    }
    
    override func execute(_ closure: @escaping (Response<RafData, FetchRafDataError>) -> Void) {
        dependencies.rafServiceInteraction.getRafData(getRafDataClosure: closure)
    }
}
