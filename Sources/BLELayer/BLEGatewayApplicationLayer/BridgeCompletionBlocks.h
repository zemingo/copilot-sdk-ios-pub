//
//  BridgeCompletionBlocks.h
//  ZemingoBLELayer
//
//  Created by Adaya on 28/11/2016.
//  Copyright © 2016 Adaya. All rights reserved.
//

#ifndef BridgeCompletionBlocks_h
#define BridgeCompletionBlocks_h
@class PeripheralInfo;

typedef void (^BLEStatusListener)(BOOL available, NSError *error);

typedef void (^ConnectionCompletion)(BOOL succeeded, NSError *error);

typedef void (^SetValue)(BOOL succeeded, NSError *error);
typedef void (^GetBoolValue)(BOOL value, NSError *error);
typedef void (^CommandSent)(NSData* result, NSError *error);

typedef void (^ScanStopped)(BOOL succeeded, NSError *error);
typedef void (^DeviceFound)(PeripheralInfo *info);


#endif /* BridgeCompletionBlocks_h */
