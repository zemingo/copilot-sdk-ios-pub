//
//  BLERequirements.m
//  FlirNvrDvr
//
//  Created by Adaya on 3/24/16.
//  Copyright © 2016 Zemingo. All rights reserved.
//

#import "BLEServiceRequirements.h"


@interface BLEServiceRequirements ()
@property (nonatomic, strong, readwrite) NSArray* charectaristicIds;
@property (nonatomic, strong, readwrite) CBUUID* serviceId;

@end

@implementation BLEServiceRequirements

- (instancetype)initWithId:(NSString*) serviceId andCharechtaristics: (NSArray*) charectaristicIds
{
    self = [super init];
    if (self) {
        NSMutableArray* array = [NSMutableArray array];
        for (NSString* charec in charectaristicIds) {
            [array addObject:[CBUUID UUIDWithString: charec]];
        }
        self.charectaristicIds = array;
        self.serviceId = [CBUUID UUIDWithString: serviceId];
    }
    return self;
}


@end
