//
//  BLEPeripheralIdle.m
//  FlirNvrDvr
//
//  Created by Adaya on 3/22/16.
//  Copyright © 2016 Zemingo. All rights reserved.
//

#import "BLEPeripheralIdle.h"
#import "BLEPeripheralErrors.h"
#import "BLEPeripheralConnecting.h"

@implementation BLEPeripheralIdle


- (id<BLEPeripheralConnectionState>) connectToDevice:(CBPeripheral*) peripheral usingManager: (CBCentralManager *) centralManager andRequiredServices:(NSArray *)bleServiceRequirements{
    return [[BLEPeripheralConnecting alloc] initWithPeripheral:peripheral usingCentralManager:centralManager andServiceRequirements:bleServiceRequirements];
}

-(id<BLEPeripheralConnectionState>)connected:(CBPeripheral *)peripheral{
    return nil;
}

-(id<BLEPeripheralConnectionState>)servicesDiscovered:(CBPeripheral *)peripheral withError:(NSError *)error transitionFailure:(NSError *__autoreleasing *)failure{
    return nil;
}

-(id<BLEPeripheralConnectionState>)charectaristicsDiscovered:(CBPeripheral *)peripheral forService:(CBService *)service withError:(NSError *)error transitionFailure:(NSError *__autoreleasing *)failure{
    return nil;
}



- (id<BLEPeripheralConnectionState>) disconnectUsing: (CBCentralManager*) centralManager{
    return nil;
}

- (id<BLEPeripheralConnectionState>) connectionFailure:(CBPeripheral*) peripheral{
    return nil;
}

- (id<BLEPeripheralConnectionState>) discoveryFailure:(CBPeripheral*) peripheral{
    return nil;
}

- (id<BLEPeripheralConnectionState>) autoDisconnected:(CBPeripheral*) peripheral{
    return nil;
}

- (id<BLEPeripheralConnectionState>) unExpectedDisconnection{
    return nil;
}

-(BLEPeripheralStates)state{
    return kBLEPeripheralIdle;
}

-(BOOL)isConnected{
    return NO;
}

-(NSSet *)availableCharectaristics{
    return nil;
}


-(NSError *)writeValue:(NSData *)value forCharacteristic:(NSString *)characteristicID shouldWaitForResponse:(BOOL *)shouldWaitForResponse{
    return [NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorInvalidCurrentState userInfo:nil];
}

-(NSError *)getValueForCharacteristic:(NSString *)characteristicID{
    return [NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorInvalidCurrentState userInfo:nil];
}

-(NSError *)registerForCharacteristic:(NSString *)characteristicID{
    return [NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorInvalidCurrentState userInfo:nil];
}

-(NSError *)unregisterForCharacteristic:(NSString *)characteristicID{
    return [NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorInvalidCurrentState userInfo:nil];
}

@end
