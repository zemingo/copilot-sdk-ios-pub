//
//  BLEPeripheralIdle.h
//  FlirNvrDvr
//
//  Created by Adaya on 3/22/16.
//  Copyright © 2016 Zemingo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLEPeripheralConnectionState.h"

@interface BLEPeripheralIdle : NSObject<BLEPeripheralConnectionState>

@end
