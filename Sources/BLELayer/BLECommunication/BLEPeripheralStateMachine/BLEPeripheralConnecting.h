//
//  BLEPeripheralConnecting.h
//  FlirNvrDvr
//
//  Created by Adaya on 3/22/16.
//  Copyright © 2016 Zemingo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLEPeripheralConnectionState.h"

@interface BLEPeripheralConnecting : NSObject<BLEPeripheralConnectionState>

-(instancetype)initWithPeripheral: (CBPeripheral *) peripheral usingCentralManager: (CBCentralManager*) centralManager andServiceRequirements: (NSArray*) serviceRequirements;

@end
