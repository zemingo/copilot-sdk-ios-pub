//
//  BLEPeripheralDisconnect.m
//  FlirNvrDvr
//
//  Created by Adaya on 3/23/16.
//  Copyright © 2016 Zemingo. All rights reserved.
//

#import "BLEPeripheralDisconnect.h"
#import "BLEPeripheralIdle.h"
#import "BLEPeripheralErrors.h"

@interface BLEPeripheralDisconnect ()

@property (strong, nonatomic) CBPeripheral* peripheral;

@end

@implementation BLEPeripheralDisconnect

- (instancetype)initWithPeripheral: (CBPeripheral *) peripheral andCentralManager:(CBCentralManager*) centralManager
{
    self = [super init];
    if (self) {
        self.peripheral = peripheral;
        [centralManager cancelPeripheralConnection: peripheral];
    }
    return self;
}


- (BOOL) isSamePeripheral:(CBPeripheral*) peripheral{
    return ([[peripheral.identifier UUIDString] caseInsensitiveCompare:[self.peripheral.identifier UUIDString]] == NSOrderedSame);
}

- (id<BLEPeripheralConnectionState>) connectToDevice: (CBPeripheral *) peripheral usingManager: (CBCentralManager *) centralManager andRequiredServices:(NSArray *)bleServiceRequirements{
    return nil;
}

-(id<BLEPeripheralConnectionState>)connected:(CBPeripheral *)peripheral{
    return nil;
}

-(id<BLEPeripheralConnectionState>)servicesDiscovered:(CBPeripheral *)peripheral withError:(NSError *)error transitionFailure:(NSError *__autoreleasing *)failure{
    return nil;
}

-(id<BLEPeripheralConnectionState>)charectaristicsDiscovered:(CBPeripheral *)peripheral forService:(CBService *)service withError:(NSError *)error transitionFailure:(NSError *__autoreleasing *)failure{
    return nil;
}

- (id<BLEPeripheralConnectionState>) disconnectUsing:(CBCentralManager *)centralManager{
    return self;
}

- (id<BLEPeripheralConnectionState>) connectionFailure: (CBPeripheral *) peripheral{
    if([self isSamePeripheral:peripheral]){
        return [[BLEPeripheralIdle alloc] init];
    }
    return nil;
}

- (id<BLEPeripheralConnectionState>) discoveryFailure: (CBPeripheral *) peripheral{
    if([self isSamePeripheral:peripheral]){
        return [[BLEPeripheralIdle alloc] init];
    }
    return nil;
}

- (id<BLEPeripheralConnectionState>) autoDisconnected: (CBPeripheral *) peripheral{
    if([self isSamePeripheral:peripheral]){
        return [[BLEPeripheralIdle alloc] init];
    }
    return nil;
}

- (id<BLEPeripheralConnectionState>) unExpectedDisconnection{
    // same as auto but there is no need verify peripheral
    return [[BLEPeripheralIdle alloc] init];
}

-(BLEPeripheralStates)state{
    return kBLEPeripheralDisconnecting;
}


-(NSSet *)availableCharectaristics{
    return nil;
}

-(BOOL)isConnected{
    return NO;
}


-(NSError *)writeValue:(NSData *)value forCharacteristic:(NSString *)characteristicID shouldWaitForResponse:(BOOL *)shouldWaitForResponse{
    return [NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorInvalidCurrentState userInfo:nil];
}

-(NSError *)getValueForCharacteristic:(NSString *)characteristicID{
    return [NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorInvalidCurrentState userInfo:nil];
}

-(NSError *)registerForCharacteristic:(NSString *)characteristicID{
    return [NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorInvalidCurrentState userInfo:nil];
}

-(NSError *)unregisterForCharacteristic:(NSString *)characteristicID{
    return [NSError errorWithDomain:BLEManagerErrorDomainName code:BLEManagerErrorInvalidCurrentState userInfo:nil];
}


@end
