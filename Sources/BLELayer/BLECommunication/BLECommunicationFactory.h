//
//  ESC_BT_Proxy.h
//  ESC_BT_Proxy
//
//  Created by Elad Urson on 5/1/16.
//  Copyright © 2016 Elad Urson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLEPeripheralErrors.h"
#import "BLEDiscovererProtocol.h"
#import "BLEPeripheralProtocol.h"
#import "BLEStatusDelegateProtocol.h"

//#define kBLEPeripheralWantedUUID                               @"FFE0"
//#define kBLERelevantServicesUUID                             @[@"FFE0"]
//#define kBLERelevantCharectaristicUUID                       @[@"FFE1",@"FFE1"]


/*
 public static final UUID UUID_SERVICE_CONNECTIVITY = UUID.fromString("000008c0-0000-1000-8000-00805f9b34fb");
 public static final UUID UUID_CHARACTERISTIC_GET_WIFI_LIST = UUID.fromString("00004110-0000-1000-8000-00805f9b34fb");//indicate
 public static final UUID UUID_CHARACTERISTIC_CONNECTION_STATUS = UUID.fromString("00004114-0000-1000-8000-00805f9b34fb");//notification
 public static final UUID UUID_CHARACTERISTIC_SET_WIFI = UUID.fromString("00004112-0000-1000-8000-00805f9b34fb");//write
 
 public static final UUID UUID_SERVICE_SETUP = UUID.fromString("000008C4-0000-1000-8000-00805f9b34fb");;//write
 public static final UUID UUID_CHARACTERISTIC_SET_TIME_ZONE = UUID.fromString("00004210-0000-1000-8000-00805f9b34fb");;//write
 public static final UUID UUID_CHARACTERISTIC_SET_TIME = UUID.fromString("00004212-0000-1000-8000-00805f9b34fb");;//write
 public static final UUID UUID_CHARACTERISTIC_SET_CAMERA_CREDENTIALS = UUID.fromString("00004214-0000-1000-8000-00805f9b34fb");//write
 
 public static final UUID UUID_SERVICE_ACTIONS = UUID.fromString("000008C8-0000-1000-8000-00805f9b34fb");
 public static final UUID UUID_CHARACTERISTIC_GO_CLOUD = UUID.fromString("00004310-0000-1000-8000-00805f9b34fb");//? NOT IMPLEMENTED on camera
 public static final UUID UUID_CHARACTERISTIC_FINDER = UUID.fromString("00004311-0000-1000-8000-00805f9b34fb");//indication NOT IMPLEMENTED on camera
 */



//#define kBLEConnectivityService @"08c0"//@"000008c0-0000-1000-8000-00805f9b34fb"
//#define kBLEConnectivityGetWifi @"4110"//@"00004110-0000-1000-8000-00805f9b34fb"
//#define kBLEConnectivityConnectionStatus @"4114"//@"00004114-0000-1000-8000-00805f9b34fb"
//#define kBLEConnectivitySetWifi @"4112"//@"00004112-0000-1000-8000-00805f9b34fb"
//#define kBLEConnectivityServiceCharectaristics @[kBLEConnectivityGetWifi, kBLEConnectivitySetWifi, kBLEConnectivityConnectionStatus]


@interface BLECommunicationFactory : NSObject

+ (BLECommunicationFactory *) sharedManager; //it is a singleton because there is only one delegate to the centralManager. there can be only one central manager see: https://forums.developer.apple.com/thread/20810

- (id <BLEDiscovererProtocol>)createBleDiscovererWithDiscoveryServices:(NSArray *)discoveryServices delegate:(id<BLEPeripheralDiscovererDelegate>)delegate;
- (id<BLEPeripheralProtocol>)connectToPeripheralWithPeripheralIdentifier:(NSUUID *) identifier requiredServices:(NSArray *)requiredServices peripheralDelegate:(id<BLEPeripheralDelegate>) periphrralDelegate;

    //BLE STATUE
- (void)addStatusDelelate:(id<BLEStatusDelegate>)statusDelegate;
- (void)removeStatusDelegate:(id<BLEStatusDelegate>)statusDelegate;
- (BOOL) isBLEOn;



@end
