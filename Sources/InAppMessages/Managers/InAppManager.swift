//
//  InAppManager.swift
//  CopilotAPIAccess
//
//  Created by Elad on 13/01/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation
import CopilotLogger

class InAppManager: InAppManagerAPIAccess, SessionLifeTimeObserver {
    
    private struct Consts {
        static let triggerTimerTimeInterval: TimeInterval = 30
    }
    
    enum InAppActivatedStatus {
        case enable
        case disable
    }
    
    typealias Dependencies = HasReporter & HasInAppServiceInteraction
    private var dependencies: Dependencies
        
    let sessionLifeTimeProvider = SessionLifeTimeProvider()

    private var inAppServerActiveated : InAppActivatedStatus = .disable
    private var inAppUserActivated: InAppActivatedStatus = .enable
    
    private var touchPolicyValidator: TouchPolicyValidator = TouchPolicyValidatorInactive()
    private var configurationManager: InAppConfigurationManagerApi
    private var fetcher: InAppFetcher = InAppFetcherManagerInactive()
    private var inbox: InAppInboxInteractable = InAppInboxInactive()
    private var render: InAppRenderer = InAppRenderingManagerInactive()
    private var reporter: InAppReporter = InAppReporterManagerInactive()
    
    private var triggerTimer: Timer?
    
    @AtomicWrite private var isInSession =  false
    
    //MARK: - Initializer
    init(reportApiAccess: ReportAPIAccess, authenticationProvider: AuthenticationProvider) {
        
        
        let bundleConfigurationProvider: ConfigurationProvider = BundleConfigurationProvider()
        
        let inAppServiceInteraction: InAppServiceInteraction = InAppServiceInteraction(authenticationProvider: authenticationProvider, configurationProvider: bundleConfigurationProvider)
        
        self.dependencies = InAppDependencies(inAppServiceInteraction: inAppServiceInteraction, reporter: reportApiAccess)
        configurationManager = InAppConfigurationManager(dependencies: dependencies)
        
        //configuration fetched
        configurationManager.configutationRecieved = {[weak self] (configuration) in
            self?.configurationRetrieved(configuration: configuration)
        }
        
        //session awareness
        sessionLifeTimeProvider.add(observer: self)
        
        if authenticationProvider.isLoggedIn {
            sessionStarted()
        }
        
        //application did become active
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        //application will resign active
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillResignActive), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    
    //MARK: - InAppMessages API
    
    public func enable() {
        ZLogManagerWrapper.sharedInstance.logInfo(message: "In app enabled")
        inAppUserActivated = .enable
        readyToTriggerWhenAvailable()
    }
    
    public func disable() {
        ZLogManagerWrapper.sharedInstance.logInfo(message: "In app disabled")
        inAppUserActivated = .disable
    }
    
    //MARK: - SessionLifeTime notifyer
    
    //NOTE! that userId may not exist at this point (because "me" request may not be completed yet)
    func sessionStarted() {
        if isInSession == false {
            ZLogManagerWrapper.sharedInstance.logInfo(message: "session started")
            isInSession = true
            configurationManager.start()
        } else {
            ZLogManagerWrapper.sharedInstance.logInfo(message: "alrady in session")
        }
        
    }
    
    func sessionEnded() {
        ZLogManagerWrapper.sharedInstance.logInfo(message: "session ended")
        isInSession = false
        configurationManager.clear()
        applyInactiveState()
    }

    //MARK: - Private
    
    private func applyActiveState() {
        
        fetcher = InAppFetcherManager(dependencies: dependencies)
        //messages fetched
        fetcher.messagesArrived = {[weak self] (messages) in
            self?.messagesArrived(messages: messages)
        }
        
        inbox = InAppInbox()
        //message triggered
        inbox.messageTriggered = { [weak self] (message) in
            self?.messageTriggered(message: message)
        }
        
        render = InAppRenderingManager()
        
        reporter = InAppReporterManager(dependencies: dependencies)
        
        //register to analytics events
        AnalyticsEventsManager.sharedInstance.eventsDispatcher.eventsListener = self
        
        touchPolicyValidator = DefaultTouchPolicyValidator()
        
        startTriggerTimer()
    }
    
    private func applyInactiveState() {
        
        AnalyticsEventsManager.sharedInstance.eventsDispatcher.eventsListener = nil
        
        fetcher.stop()
        inbox.resetInbox()
        
        reporter = InAppReporterManagerInactive()
        inbox = InAppInboxInactive()
        render = InAppRenderingManagerInactive()
        touchPolicyValidator = TouchPolicyValidatorInactive()
        fetcher = InAppFetcherManagerInactive()
        stopTriggerTimer()
    }
    
    private func messagesArrived(messages: [InAppMessage]) {
        inbox.populateMessages(messages)
        ZLogManagerWrapper.sharedInstance.logInfo(message: "Retrieved in app message list with size \(messages.count)")
    }
    
    private func configurationRetrieved(configuration: InAppMessagesConfiguration) {
        if configuration.activated {
            applyActiveState()
            inAppServerActiveated = .enable
            touchPolicyValidator = configuration.thouchPolicy
            fetcher.pollingInterval = configuration.pollingInterval
            fetcher.start()
        } else {
            inAppServerActiveated = .disable
            applyInactiveState()
        }
    }
    
    private func messageTriggered(message: InAppMessage) {
        ZLogManagerWrapper.sharedInstance.logInfo(message: "message with \(message.id) messageId was triggered")
        reporter.reportMessageTriggered(generalParameters: message.report.parameters)
        if inAppUserActivated == .enable {
            if touchPolicyValidator.canInteractWithUser() {
                render.render(inAppMessage: message, reporter: reporter) { [weak self] (success) in
                    if success {
                        self?.reporter.reportMessageDisplayed(generalParameters: message.report.parameters)
                        message.markAsEvicted()
                        self?.touchPolicyValidator.setInteracted()
                        self?.dependencies.inAppServiceInteraction.postMessageDisplayed(messageId: message.id) { (result) in
                            //TODO: handle errors
                            switch result {
                            case .success(let status):
                                ZLogManagerWrapper.sharedInstance.logInfo(message: "message displayed result success with status: \(status)")
                            case .failure(let error):
                                ZLogManagerWrapper.sharedInstance.logError(message: "message displayed result failure with error: \(error.errorDescription ?? "no error description")")
                            }
                        }
                    } else {
                        //message didn't displayed
                    }
                }
            } else {
                ZLogManagerWrapper.sharedInstance.logInfo(message: "message with \(message.id) wont be dispatched - touch policy blocked")
            }
        } else {
            ZLogManagerWrapper.sharedInstance.logInfo(message: "In app disabled")
        }
    }
    
    private func readyToTriggerWhenAvailable() {
        if isInSession {
            inbox.readyToTrigger()
        } else {
            ZLogManagerWrapper.sharedInstance.logInfo(message: "application in foreground - but there is no active session")
        }
    }
    
    private func stopTriggerTimer() {
        triggerTimer?.invalidate()
        triggerTimer = nil
    }
    
    private func startTriggerTimer() {
        
        guard triggerTimer == nil else {
            ZLogManagerWrapper.sharedInstance.logError(message: "trigger timer is already active")
            return
        }
        DispatchQueue.main.async { [weak self] in
            self?.triggerTimer = Timer.scheduledTimer(withTimeInterval: Consts.triggerTimerTimeInterval, repeats: true, block: { (_) in
                self?.readyToTriggerWhenAvailable()
            })
        }
    }
    
    @objc private func applicationDidBecomeActive() {
        ZLogManagerWrapper.sharedInstance.logInfo(message: "application did become active")
        if isInSession && inAppServerActiveated == .enable {
            configurationManager.start()
            fetcher.start()
            readyToTriggerWhenAvailable()
            startTriggerTimer()
        }
        else {
            ZLogManagerWrapper.sharedInstance.logInfo(message: "application in foreground but there is no active session or Inapp feature is not activated")
        }
    }
    
    @objc private func applicationWillResignActive() {
        ZLogManagerWrapper.sharedInstance.logInfo(message: "application did enter background")
        fetcher.stop()
        stopTriggerTimer()
        configurationManager.clear()
    }
    
}

extension InAppManager: AnalyticsEventReceiver {
    func eventLogged(_ event: AnalyticsEvent) {
        if let _ = event as? CopilotSystemEvent {
            ZLogManagerWrapper.sharedInstance.logInfo(message: "Analytics event ignored - system event")
        } else {
            inbox.analyticsEventReceived(event)
        }
    }
}
