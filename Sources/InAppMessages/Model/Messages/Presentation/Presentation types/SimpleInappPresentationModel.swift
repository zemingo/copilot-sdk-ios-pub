//
//  SimpleInappPresentationModel.swift
//  CopilotAPIAccess
//
//  Created by Elad on 28/01/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation


struct SimpleInappPresentationModel {
    
    //MARK: - Consts
    private struct Keys {
        static let title = "title"
        static let body = "body"
        static let imageUrl = "imageUrl"
        static let ctas = "ctas"
    }
    
    //MARK: - Properties
        let title: String
        let body: String?
        let imageUrl: String?
        let ctas: [CtaType?]
    
    // MARK: - Init
    init?(withDictionary dictionary: [String: Any]) {
        guard let title = dictionary[Keys.title] as? String,
            let ctasArr = dictionary[Keys.ctas] as? [[String : AnyObject]], 1...3 ~= ctasArr.count  else { return nil }
        
        var ctasFromJson: [CtaType?] = []
        ctasArr.forEach { ctasFromJson.append(CtaTypeMapper.map(withDictionary: $0)) }
        if ctasFromJson.contains(where: {$0 == nil}) { return nil }
        ctas = ctasFromJson
        
        self.title = title
        
        if let body = dictionary[Keys.body] as? String { self.body = body } else { self.body = nil }
        if let imageUrl = dictionary[Keys.imageUrl] as? String { self.imageUrl = imageUrl } else { self.imageUrl = nil }

    }
}

extension SimpleInappPresentationModel: InAppMessagePresentation {    
    var renderer: Renderer { SimpleInappRenderer(presentationModel: self) }
}
