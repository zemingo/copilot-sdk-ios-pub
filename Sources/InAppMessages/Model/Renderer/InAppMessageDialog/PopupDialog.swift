//
//  PopupDialog.swift
//  CopilotAPIAccess
//
//  Created by Elad on 19/02/2020.
//  Copyright © 2020 Elad. All rights reserved.
//

import Foundation
import UIKit
import SafariServices
import CopilotLogger
import StoreKit
import MessageUI
import WebKit

protocol ActionPerformer {
    func performAction(_ actionType: CtaActionType, completion: (() -> ())?)
}

/// Creates a Popup dialog similar to UIAlertController
class PopupDialog: UIViewController {
    
    // MARK: Private / Internal
    
    /// First init flag
    fileprivate var initialized = false
    
    /// StatusBar display related
    fileprivate let hideStatusBar: Bool
    fileprivate var statusBarShouldBeHidden: Bool = false
    
    /// Width for iPad displays
    fileprivate let preferredWidth: CGFloat
    
    
    ///for html popup - different layout
    fileprivate let isHtmlPopup: Bool
    
    /// The completion handler
    fileprivate var completion: (() -> Void)?
    
    /// The custom transition presentation manager
    fileprivate var presentationManager: PresentationManager!
    
    /// Returns the controllers view
    var popupContainerView: PopupDialogContainerView {
        return view as! PopupDialogContainerView // swiftlint:disable:this force_cast
    }
    
    
    /// The set of buttons
    fileprivate var buttons = [InAppButton]()
    
    ///The set of htmlButtons
    var htmlButtons = [CtaHtmlType]()
    
    var htmlButtonCompletion: (() -> ())?
    
    ///html cta press indicator
    var htmlCtaPressed: (() -> Void)?
    /// The content view of the popup dialog
    var viewController: UIViewController
    
    //safari view controller
    private var safariVC: SFSafariViewController!
    
    //for presenting the popup on new window
    private lazy var window: UIWindow = {
        let win = UIWindow(frame: UIScreen.main.bounds)
        let root = UIViewController()
        root.view.backgroundColor = .clear
        win.rootViewController = root
        win.windowLevel = .normal
        return win
    }()
    
    // MARK: - Initializers
    
    /*!
     Creates a standard popup dialog with title, message and image field
     
     - parameter title:            The dialog title
     - parameter message:          The dialog message
     - parameter image:            The dialog image
     - parameter buttonAlignment:  The dialog button alignment
     - parameter transitionStyle:  The dialog transition style
     - parameter preferredWidth:   The preferred width for iPad screens
     - parameter hideStatusBar:    Whether to hide the status bar on PopupDialog presentation
     - parameter completion:       Completion block invoked when dialog was dismissed
     
     - returns: Popup dialog default style
     */
    convenience init(
        title: String?,
        message: String?,
        image: UIImage? = nil,
        buttonAlignment: NSLayoutConstraint.Axis = .vertical,
        transitionStyle: PopupDialogTransitionStyle = .bounceUp,
        preferredWidth: CGFloat = 340,
        hideStatusBar: Bool = false,
        isHtmlPopup: Bool = false,
        completion: (() -> Void)? = nil) {
        
        // Create and configure the standard popup dialog view
        let viewController = PopupDialogViewController()
        viewController.titleText   = title
        viewController.messageText = message
        viewController.image       = image
        
        // Call designated initializer
        self.init(viewController: viewController,
                  buttonAlignment: buttonAlignment,
                  transitionStyle: transitionStyle,
                  preferredWidth: preferredWidth,
                  hideStatusBar: hideStatusBar,
                  isHtmlPopup: isHtmlPopup,
                  completion: completion)
    }
    
    //for html popup view
    convenience init(htmlContent: String,
                     preferredWidth: CGFloat = 340,
                     hideStatusBar: Bool = false,
                     isHtmlPopup: Bool = true,
                     completion: (() -> Void)? = nil) {
        
        let viewController = HtmlPopupDialogViewController()
        viewController.webContent = htmlContent        
         //Call designated initializer
        self.init(viewController: viewController,
                  preferredWidth: preferredWidth,
                  hideStatusBar: hideStatusBar,
                  isHtmlPopup: isHtmlPopup,
                  completion: completion)
        viewController.htmlView.webview.navigationDelegate = self
    }
    /*!
     Creates a popup dialog containing a custom view
     
     - parameter viewController:   A custom view controller to be displayed
     - parameter buttonAlignment:  The dialog button alignment
     - parameter transitionStyle:  The dialog transition style
     - parameter preferredWidth:   The preferred width for iPad screens
     - parameter hideStatusBar:    Whether to hide the status bar on PopupDialog presentation
     - parameter completion:       Completion block invoked when dialog was dismissed
     
     - returns: Popup dialog with a custom view controller
     */
    init(
        viewController: UIViewController,
        buttonAlignment: NSLayoutConstraint.Axis = .vertical,
        transitionStyle: PopupDialogTransitionStyle = .bounceUp,
        preferredWidth: CGFloat = 340,
        hideStatusBar: Bool = false,
        isHtmlPopup: Bool,
        completion: (() -> Void)? = nil) {
        
        self.viewController = viewController
        self.preferredWidth = preferredWidth
        self.hideStatusBar = hideStatusBar
        self.completion = completion
        self.isHtmlPopup = isHtmlPopup
        
        super.init(nibName: nil, bundle: nil)
        
        // Init the presentation manager
        presentationManager = PresentationManager(transitionStyle: transitionStyle)
        
        
        // Define presentation styles
        transitioningDelegate = presentationManager
        modalPresentationStyle = .custom
        
        // StatusBar setup
        modalPresentationCapturesStatusBarAppearance = true
        
        // Add our custom view to the container
        addChild(viewController)
        popupContainerView.mainStackView.insertArrangedSubview(viewController.view, at: 0)
        viewController.didMove(toParent: self)
    }
    
    // Init with coder not implemented
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View life cycle
    
    /// Replaces controller view with popup view
    override func loadView() {
        view = PopupDialogContainerView(frame: UIScreen.main.bounds, preferredWidth: preferredWidth, isHtmlPopup: isHtmlPopup)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard !initialized else { return }
        appendButtons()
        initialized = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        statusBarShouldBeHidden = hideStatusBar
        UIView.animate(withDuration: 0.15) {
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    deinit {
        completion?()
        completion = nil
    }
    
    // MARK: - Dismissal related
    
    /*!
     Dismisses the popup dialog
     */
    func dismiss(_ completion: (() -> Void)? = nil) {
        self.dismiss(animated: true) {
            self.window.dismiss()
            completion?()
        }
    }
    
    // MARK: - Button related
    
    /*!
     Appends the buttons added to the popup dialog
     to the placeholder stack view
     */
    fileprivate func appendButtons() {
        
        // Add action to buttons if the viewController is kind of PopupDialogViewController
        if let viewController = viewController as? PopupDialogViewController {
            if buttons.isEmpty {
                viewController.buttons = []
                return
            }
            viewController.buttons = buttons
            buttons.forEach {
                $0.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
            }
        }
    }
    
    /*!
     Adds a single InAppButton to the Popup dialog
     - parameter button: A InAppButton instance
     */
    func addButton(_ button: InAppButton) {
        buttons.append(button)
    }
    
    /*!
     Adds an array of InAppButtons to the Popup dialog
     - parameter buttons: A list of InAppButton instances
     */
    func addButtons(_ buttons: [InAppButton]) {
        self.buttons += buttons
    }
    
    /// Calls the action closure of the button instance tapped
    @objc fileprivate func buttonTapped(_ button: InAppButton) {
        button.buttonAction?()
    }
    
    
    // MARK: - StatusBar display related
    
    override var prefersStatusBarHidden: Bool {
        return statusBarShouldBeHidden
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    //MARK: - Show popupDialog on window
    
    func show(animated: Bool = true, completion: (() -> Void)? = nil)  {
        if let rootViewController = window.rootViewController {
            window.makeKeyAndVisible()
            rootViewController.present(self, animated: animated, completion: completion)
        }
    }
    //MARK: - transition
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
}

// MARK: - View proxy values

extension PopupDialog {
    
    /// The transition style
    var transitionStyle: PopupDialogTransitionStyle {
        get { return presentationManager.transitionStyle }
        set { presentationManager.transitionStyle = newValue }
    }
}

extension PopupDialog: ActionPerformer {
    
    func performAction(_ actionType: CtaActionType, completion: (() -> ())?) {
        popupContainerView.isHidden = true
        switch actionType {
        case .appStoreRate:
            rateOnAppstore()
        case .webNavigation(let url):
            if let url = URL(string: url) {
                openSafariVC(with: url, completion: completion)
            } else {
                ZLogManagerWrapper.sharedInstance.logError(message: "cta url is invalid")
            }
        case .call(let phoneNumber):
            callTo(number: phoneNumber)
        case .sendEmail(let meilTo, let subject, let body):
            sendEmail(to: meilTo, subject: subject, body: body)
        case .share(let shareText):
            share(text: shareText)
        case .none:
            self.dismiss()
        }
        completion?()
    }
    
    private func share(text: String?) {
        let items = [text]
        let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        activityController.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
            // User completed activity
            self.dismiss()
        }
        present(activityController, animated: true)
    }
    
    private func openSafariVC(with url: URL, completion: (() -> ())?) {
        safariVC = SFSafariViewController(url: url)
        safariVC.delegate = self
        self.present(safariVC, animated: true, completion: {
            completion?()
        })
    }
    
    private func rateOnAppstore() {
        if #available( iOS 10.3,*){
            SKStoreReviewController.requestReview()
            self.dismiss()
        }
    }
    
    private func callTo(number: String) {
        guard let numberToCall = URL(string: "tel://" + number) else { return }
        UIApplication.shared.open(numberToCall)
        self.dismiss()
    }
    
    private func sendEmail(to: String?, subject: String?, body: String?) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            if let sendTo = to {
                mail.setToRecipients([sendTo])
            }
            if let messageBody = body {
                mail.setMessageBody(messageBody, isHTML: false)
            }
            mail.setSubject(subject ?? "")
            
            present(mail, animated: true)
        } else {
            ZLogManagerWrapper.sharedInstance.logError(message: "can't send email")
            dismiss()
        }
    }
}

//MARK: - MFMailComposeViewControllerDelegate
extension PopupDialog: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss()
    }
}

//MARK: - SFSafariViewControllerDelegate
extension PopupDialog: SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        self.dismiss()
    }
}

extension PopupDialog: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let navigationActionUrl = navigationAction.request.url, navigationActionUrl.absoluteString.contains("cplt://") {
            htmlCtaPressed?()
            let actionString = navigationActionUrl.absoluteString.replacingOccurrences(of: "cplt://", with: "")
            performHtmlAction(with: actionString)
        }
        decisionHandler(.allow)
    }
    
    private func performHtmlAction(with urlActionString: String) {
        htmlButtons.forEach { [weak self] in
            if $0.redirectId == urlActionString {
                performAction($0.action) {
                    self?.htmlButtonCompletion?()
                }
            }
        }
    }
}
