//
//  HtmlPopupDialogViewController.swift
//  CopilotAPIAccess
//
//  Created by Elad on 05/03/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation
import WebKit

class HtmlPopupDialogViewController: UIViewController {

    var htmlView: HtmlDialogDefaultView {
       return view as! HtmlDialogDefaultView // swiftlint:disable:this force_cast
    }

    override func loadView() {
        super.loadView()
        view = HtmlDialogDefaultView(frame: .zero)
    }
}

extension HtmlPopupDialogViewController {
    // MARK: Content
    
    var webContent: String? {
        get { return htmlView.webContent }
        set { htmlView.webContent = newValue }
    }
}
