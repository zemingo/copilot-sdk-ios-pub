//
//  CopilotLogger.h
//  CopilotLogger
//
//  Created by Richard Houta on 17/09/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CopilotLogger.
FOUNDATION_EXPORT double CopilotLoggerVersionNumber;

//! Project version string for CopilotLogger.
FOUNDATION_EXPORT const unsigned char CopilotLoggerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CopilotLogger/PublicHeader.h>

#import <CopilotLogger/ZLogManager.h>
