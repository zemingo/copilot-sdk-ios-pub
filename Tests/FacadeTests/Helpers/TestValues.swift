//
//  TestValues.swift
//  CopilotAPIAccessTests
//
//  Created by Ofer Meroz on 19/11/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import Foundation
@testable import CopilotAPIAccess

var testUser: UserMe {
    // userDetails
    let id = "123"
    let email = testEmailAddress
    
    // info
    let firstName = testFirstName
    let lastName = testLastName
    
    let info: [String : Any] = ["firstName" : firstName,
                                "lastName" : lastName]
    
    let userDetails: [String : Any] = ["id" : id,
                                       "email" : email,
                                       "info" : info]
    
    let accountStatus: [String : Any] = ["termsOfUseApproved" : true,
                                         "credentialsType" : CredentialsType.anonymous.rawValue,
                                         "emailVerificationStatus" : EmailVerificationStatus.verified]
    
    return UserMe(withDictionary: ["userDetails" : userDetails, "accountStatus" : accountStatus])!
}

let testEmailAddress = "test@copilot.com"
let testPassword = "01201930"
let testFirstName = "Buzz"
let testLastName = "Aldrin"
let testConsentName = "TestConsent"
let testConsent = [testConsentName : true, "analytics" : true]
let testNewPassword = "something"
