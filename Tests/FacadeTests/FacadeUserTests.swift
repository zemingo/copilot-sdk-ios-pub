//
//  FacadeUserTests.swift
//  CopilotAPIAccessTests
//
//  Created by Ofer Meroz on 13/11/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class FacadeUserTests: XCTestCase {
    
    static let testTermsOfUseVersion = "1"
    static let testPNSToken = "testPNSToken".data(using: .utf8)!
    
    // custom settings values
    static let testLanguageKey = "language"
    static let testLanguage = Language.english
    static let testDefaultAddressKey = "defaultAddress"
    static let testDefaultAddress = Address(city: "Manhattan", street: "42nd", homeNumber: 42)
    
    private var userAPI: UserAPIAccess!
    
    override func setUp() {
        super.setUp()
        
        let dependencies = TestUserDependencies(userServiceInteraction: TestUserServiceInteraction(), authenticationServiceInteraction: UserTestsAuthenticationServiceInteraction(), reporter: DummyReportAPI())
        userAPI = UserAPI(dependencies: dependencies)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // MARK: - Tests

    func testFetchMe() {
        let expectation = self.expectation(description: "me call closure should be executed")
        
        userAPI
            .fetchMe()
            .build()
            .execute({ (response) in
                switch response {
                case .success(let userMe):
                    XCTAssertEqual(userMe.userDetails.id, testUser.userDetails.id)
                    break
                default:
                    break
                }
                expectation.fulfill()
            })
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testElevateAnonymousUser() {
        let expectation = self.expectation(description: "elevateAnonymous call closure should be executed")
        
        userAPI
            .elevate()
            .with(email: testEmailAddress, password: testPassword, firstname: testFirstName, lastname: testLastName)
            .build()
            .execute { (_) in
                expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testUpdateMeWithFirstNameAndLastNameAndCustomSettings() {
        let expectation = self.expectation(description: "updateUser call closure should be executed")
        
        userAPI
            .updateMe()
            .with(firstname: testFirstName)
            .with(lastname: testLastName)
            .with(customValue: FacadeUserTests.testLanguage, forKey: FacadeUserTests.testLanguageKey)
            .with(customValue: FacadeUserTests.testDefaultAddress, forKey: FacadeUserTests.testDefaultAddressKey)
            .build()
            .execute { (_) in
                expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testUpdateMeWithTermsOfUseApproval() {
        let expectation = self.expectation(description: "approveTermsOfUse call closure should be executed")
        
        userAPI
            .updateMe()
            .approveTermsOfUse(forVersion: FacadeUserTests.testTermsOfUseVersion)
            .build()
            .execute { (_) in
                expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
//    func testUpdateMeWithDeviceDetails() {
//        let expectation = self.expectation(description: "updateDeviceDetails call closure should be executed")
//
//        userAPI
//            .updateMe()
//            .withPushToken(pnsToken: FacadeUserTests.testPNSToken, isSandbox: true)
//            .build()
//            .execute { (_) in
//                expectation.fulfill()
//        }
//
//        waitForExpectations(timeout: 30, handler: nil)
//    }
    
    func testUpdateMeWithConsent() {
        let expectation = self.expectation(description: "setConsents call closure should be executed")
        
        userAPI
            .updateMe()
            .withCustomConsent(testConsentName, value: true)
            .allowCopilotUserAnalysis(true)
            .build()
            .execute { (_) in
                expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testChangePassword(){
        let expectation = self.expectation(description: "change password call closure should be executed")

        userAPI
            .updateMe()
            .withNewPassword(testNewPassword, verifyWithOldPassword: testPassword)
            .build()
            .execute { (_) in
                expectation.fulfill()
        }

        waitForExpectations(timeout: 30, handler: nil)
    }
    func testSendVerificationEmail(){
        let expectation = self.expectation(description: "send verification email call closure should be executed")

        userAPI
            .sendVerificationEmail()
            .build()
            .execute { (_) in
                expectation.fulfill()
        }

        waitForExpectations(timeout: 30, handler: nil)
    }
}

// MARK: - Helpers

fileprivate struct TestUserDependencies: HasUserServiceInteraction, HasAuthenticationServiceInteraction, HasReporter {
    let userServiceInteraction: UserServiceInteractable
    let authenticationServiceInteraction: AuthenticationServiceInteractable
    let reporter: ReportAPIAccess
}

fileprivate class TestUserServiceInteraction: BaseTestUserServiceInteraction {
    
    override func me(getCurrentUserClosure: @escaping GetCurrentUserClosure) {
        let res: Response<UserMe, FetchMeError> = .success(testUser)
        getCurrentUserClosure(res)
    }
    
    override func updateUser(id: String, userInfo: UserInfo?, customSettings: [String : Any]?, updateUserClosure: @escaping UpdateUserClosure) {
        XCTAssertEqual(id, testUser.userDetails.id)
        XCTAssertNotNil(userInfo)
        XCTAssertEqual(userInfo!.firstName, testFirstName)
        XCTAssertEqual(userInfo!.lastName, testLastName)
        XCTAssertNotNil(customSettings)
        XCTAssertNotNil(customSettings![FacadeUserTests.testLanguageKey])
        XCTAssertEqual(customSettings![FacadeUserTests.testLanguageKey] as! String, FacadeUserTests.testLanguage.rawValue)
        XCTAssertNotNil(customSettings![FacadeUserTests.testDefaultAddressKey])
        
        //Address
        let serializedAddressRecevied = customSettings![FacadeUserTests.testDefaultAddressKey]!
        let address = try? JSONDecoder().decode(Address.self, from: JSONSerialization.data(withJSONObject: serializedAddressRecevied, options: []))
        
        XCTAssertNotNil(address)
        XCTAssertEqual(address, FacadeUserTests.testDefaultAddress)
        
        //Language
        let serializedLanguageRecevied = customSettings![FacadeUserTests.testLanguageKey]!
        let language = try? JSONDecoder().decode(Language.self, from: JSONSerialization.data(withJSONObject: serializedLanguageRecevied, options: []))
        
        XCTAssertNotNil(language)
        XCTAssertEqual(language, FacadeUserTests.testLanguage)
        
        let res: Response<UserMe, UpdateUserDetailsError> = .success(testUser)
        updateUserClosure(res)
    }
    
    override func updateDeviceDetails(copilotSdkVersion: String, pnsToken: Data, isSandbox: Bool, updateCurrentDeviceClosure: @escaping UpdateDeviceDetailsClosure) {
        let sdkVersionFromPlist = Bundle(for: type(of: self)).object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        XCTAssertEqual(copilotSdkVersion, sdkVersionFromPlist)
        XCTAssertEqual(pnsToken, FacadeUserTests.testPNSToken)
        XCTAssertEqual(isSandbox, true)
        let res: Response<Void, UpdateDeviceDetailsError> = .success(())
        updateCurrentDeviceClosure(res)
    }
    
}

fileprivate class UserTestsAuthenticationServiceInteraction: BaseTestAuthenticationServiceInteraction {
    
    override func elevateAnonymous(withEmail email: String, password: String, firstName: String, lastName: String, elevateClosure: @escaping ElevateAnonymousClosure) {
        XCTAssertEqual(email, testEmailAddress)
        XCTAssertEqual(password, testPassword)
        XCTAssertEqual(firstName, testFirstName)
        XCTAssertEqual(lastName, testLastName)
        let res: Response<Void, ElevateAnonymousUserError> = .success(())
        elevateClosure(res)
    }
    
    override func approveTermsOfUse(for termsOfUseVersion: String, closure: @escaping ApproveTermsOfUseClosure) {
        XCTAssertEqual(termsOfUseVersion, FacadeUserTests.testTermsOfUseVersion)
        let res: Response<Void, ApproveTermsOfUseError> = .success(())
        closure(res)
    }
    
    override func setConsents(details consentsDetails: [String : Bool], closure: @escaping SetConsetClosure) {
        XCTAssertEqual(consentsDetails, testConsent)
        let res: Response<Void, UpdateUserConsentError> = .success(())
        closure(res)
    }
    override func changePassword(newPassword: String, oldPassword: String, closure: @escaping ChangePasswordClosure) {
        XCTAssertEqual(newPassword, testNewPassword)
        XCTAssertEqual(oldPassword, testPassword)
        let res: Response<Void, ChangePasswordError> = .success(())
        closure(res)
    }
    override func sendVerificationEmail(closure: @escaping SendVerificationEmailClosure) {
        let res: Response<Void, SendVerificationEmailError> = .success(())
        closure(res)
    }
}


enum Language: String, Codable {
    case english, spanish, french, german
    
    enum CodingKeys: String, CodingKey {
        case english = "english"
        case spanish = "spanish"
        case french = "french"
        case german = "german"
    }
}

struct Address {
    let city: String
    let street: String
    let homeNumber: Int
}

extension Address: Codable {
    enum CodingKeys: String, CodingKey {
        case city = "city"
        case street = "street"
        case homeNumber = "home_number"
    }
}

extension Address: Equatable {
    
    public static func == (lhs: Address, rhs: Address) -> Bool {
        return lhs.city == rhs.city && lhs.street == rhs.street && lhs.homeNumber == rhs.homeNumber
    }
}
