//
//  RejectReasonTests.swift
//  CopilotAPIAccessTests
//
//  Created by Adaya on 24/01/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class RejectReasonTests: XCTestCase {
    /*
     static let thingAlreadyAssociated = "thingAlreadyAssociated"
     static let thingNotAllowed = "thingNotAllowed"
     */
    
    func testConversionAlreadyAssociatedSimple(){
        let reason = RejectReason.fromValue(value: "thingAlreadyAssociated")
        XCTAssertEqual(RejectReason.ThingAlreadyAssociated, reason)
    }
    
    func testConversionNotAllowedSimple(){
        let reason = RejectReason.fromValue(value: "thingNotAllowed")
        XCTAssertEqual(RejectReason.ThingNotAllowed, reason)
    }
    
    func testConversionUnknownSimple(){
        let reason = RejectReason.fromValue(value: "thingNotAllowed1")
        XCTAssertEqual(RejectReason.Unknown, reason)
    }
    
    func testConversionAlreadyAssociatedPrefix(){
        let reason = RejectReason.fromValue(value: "a.thingAlreadyAssociated", ignorePrefix: "a.")
        XCTAssertEqual(RejectReason.ThingAlreadyAssociated, reason)
    }
    
    func testConversionNotAllowedPrefix(){
        let reason = RejectReason.fromValue(value: "a.thingNotAllowed", ignorePrefix: "a.")
        XCTAssertEqual(RejectReason.ThingNotAllowed, reason)
    }
    
    func testConversionUnknownPrefix(){
        let reason = RejectReason.fromValue(value: "a.thingNotAllowed1", ignorePrefix: "a.")
        XCTAssertEqual(RejectReason.Unknown, reason)
    }
    func testConversionAlreadyAssociatedWrongPrefix(){
        let reason = RejectReason.fromValue(value: "ab.thingAlreadyAssociated", ignorePrefix: "a.")
        XCTAssertEqual(RejectReason.Unknown, reason)
    }
    
    
}
