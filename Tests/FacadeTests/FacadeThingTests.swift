//
//  FacadeThingTests.swift
//  CopilotAPIAccessTests
//
//  Created by Ofer Meroz on 28/10/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class FacadeThingTests: XCTestCase {
    
    static var testThing: Thing {
        let id = "123"
        let firmware = "1.2.3"
        let model = "Vuvuzela-1"
        let physicalId = "GC1-61"
        return Thing(withDictionary: ["id" : id,
                                      "info" : ["firmware" : firmware, "model" : model, "physicalId" : physicalId]])!
    }
    static let updatedFirmware = "4.5.6"
    static let updatedName = "My Vuvuzela"
    static let updatedThingStatus = ThingStatus(lastSeen: Date(), reportedStatuses: [ThingReportedStatus]())
    
    
    private var thingAPI: ThingAPIAccess!
    
    override func setUp() {
        super.setUp()
        
        let thingsServiceInteraction = TestThingsServiceInteraction()
        let dependencies = TestThingDependencies(thingsServiceInteraction: thingsServiceInteraction)
        thingAPI = ThingAPI(dependencies: dependencies)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // MARK: - Tests
    
    func testFetchThings() {
        let expectation = self.expectation(description: "fetchThings call closure should be executed")
        
        thingAPI
            .fetchThings()
            .build()
            .execute { (_) in
                expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testFetchThing() {
        let expectation = self.expectation(description: "fetchThing call closure should be executed")
        
        thingAPI
            .fetchThing(withPhysicalId: FacadeThingTests.testThing.thingInfo.physicalId)
            .build()
            .execute { (_) in
                expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testCanAssociateThing() {
        let expectation = self.expectation(description: "associateThing call closure should be executed")
        
        let thing = FacadeThingTests.testThing
        
        thingAPI
            .checkIfCanAssociate(withPhysicalId: thing.thingInfo.physicalId)
            .build()
            .execute { (_) in
                expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testAssociateThing() {
        let expectation = self.expectation(description: "associateThing call closure should be executed")
        
        let thing = FacadeThingTests.testThing
        
        thingAPI
            .associateThing(withPhysicalId:thing.thingInfo.physicalId, firmware: thing.thingInfo.firmware, model: thing.thingInfo.model)
            .build()
            .execute { (_) in
                expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testDisassociateThing() {
        let expectation = self.expectation(description: "disassociateThing call closure should be executed")
        
        let thing = FacadeThingTests.testThing
        
        thingAPI
            .disassociateThing(withPhysicalId: thing.thingInfo.physicalId)
            .build()
            .execute { (_) in
                expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testUpdateAssociatedThingFirmwareAndNameAndThingStatus() {
        let expectation = self.expectation(description: "associateThing call closure should be executed")

        let thing = FacadeThingTests.testThing
        thingAPI
            .updateThing(withPhysicalId: thing.thingInfo.physicalId)
            .with(firmware: FacadeThingTests.updatedFirmware)
            .with(name: FacadeThingTests.updatedName)
            .with(status: FacadeThingTests.updatedThingStatus)
            .build()
            .execute { (_) in
                expectation.fulfill()
        }

        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testUpdateAssociatedThingFirmware() {
        let expectation = self.expectation(description: "associateThing call closure should be executed")
        
        let thing = FacadeThingTests.testThing
        
        let thingsServiceInteraction = TestThingsServiceInteractionUpdateFirmware()
        let dependencies = TestThingDependencies(thingsServiceInteraction: thingsServiceInteraction)
        ThingAPI(dependencies: dependencies)
            .updateThing(withPhysicalId: thing.thingInfo.physicalId)
            .with(firmware: FacadeThingTests.updatedFirmware)
            .build()
            .execute { (_) in
                expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testUpdateAssociatedThingName() {
        let expectation = self.expectation(description: "associateThing call closure should be executed")
        
        let thing = FacadeThingTests.testThing
        
        let thingsServiceInteraction = TestThingsServiceInteractionUpdateName()
        let dependencies = TestThingDependencies(thingsServiceInteraction: thingsServiceInteraction)
        ThingAPI(dependencies: dependencies)
            .updateThing(withPhysicalId: thing.thingInfo.physicalId)
            .with(name: FacadeThingTests.updatedName)
            .build()
            .execute { (_) in
                expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
}

// MARK: - Helpers

fileprivate struct TestThingDependencies: HasThingsServiceInteraction {
    let thingsServiceInteraction: ThingsServiceInteractable
}

fileprivate class TestThingsServiceInteraction: ThingsServiceInteractable {
    func disassociateThing(physicalID: String, disassociateThingClosure: @escaping DisassociateThingClosure) {
        XCTAssertEqual(physicalID, FacadeThingTests.testThing.thingInfo.physicalId)
        let res: Response<Void, DisassociateThingError> = .success(())
        disassociateThingClosure(res)
    }
    
    func canAssociateThing(physicalID: String, canAssociateThingClosure: @escaping CanAssociateThingClosure) {
        XCTAssertEqual(physicalID, FacadeThingTests.testThing.thingInfo.physicalId)
        let canAss = CanAssociateResponse()
        let res: Response<CanAssociateResponse, CanAssociateThingError> = .success(canAss)
        canAssociateThingClosure(res)
    }
    
    func getAssociatedThings(getThingsClosure: @escaping GetThingsClosure) {
        let things = [FacadeThingTests.testThing]
        let res: Response<[Thing], FetchThingsError> = .success(things)
        getThingsClosure(res)
    }
    
    func getAssociatedSingleThing(physicalID: String, getThingClosure: @escaping GetThingClosure) {
        XCTAssertEqual(physicalID, FacadeThingTests.testThing.thingInfo.physicalId)
        let res: Response<Thing, FetchSingleThingError> = .success(FacadeThingTests.testThing)
        getThingClosure(res)
    }
    
    func associateThing(firmware: String, model: String, physicalID: String, associateThingClosure: @escaping AssociateThingClosure) {
        XCTAssertEqual(firmware, FacadeThingTests.testThing.thingInfo.firmware)
        XCTAssertEqual(model, FacadeThingTests.testThing.thingInfo.model)
        XCTAssertEqual(physicalID, FacadeThingTests.testThing.thingInfo.physicalId)
        let res: Response<Thing, AssociateThingError> = .success(FacadeThingTests.testThing)
        associateThingClosure(res)
    }
    
    func updateAssociatedThing(physicalID: String, firmware: String?, name: String?, customSettings: [String : Any]?, thingStatus: ThingStatus?, updateThingClosure: @escaping UpdateThingClosure) {
        XCTAssertEqual(physicalID, FacadeThingTests.testThing.thingInfo.physicalId)
        XCTAssertEqual(firmware, FacadeThingTests.updatedFirmware)
        XCTAssertEqual(name, FacadeThingTests.updatedName)
        XCTAssertEqual(thingStatus, FacadeThingTests.updatedThingStatus)
        XCTAssertNil(customSettings)
        let res: Response<Thing, UpdateThingError> = .success(FacadeThingTests.testThing)
        updateThingClosure(res)
    }
}


fileprivate class TestThingsServiceInteractionUpdateFirmware: TestThingsServiceInteraction {
    
    override func updateAssociatedThing(physicalID: String, firmware: String?, name: String?, customSettings: [String : Any]?, thingStatus: ThingStatus?, updateThingClosure: @escaping UpdateThingClosure) {
        XCTAssertEqual(physicalID, FacadeThingTests.testThing.thingInfo.physicalId)
        XCTAssertEqual(firmware, FacadeThingTests.updatedFirmware)
        XCTAssertNil(name)
        XCTAssertNil(thingStatus)
        XCTAssertNil(customSettings)
        let res: Response<Thing, UpdateThingError> = .success(FacadeThingTests.testThing)
        updateThingClosure(res)
    }
}

fileprivate class TestThingsServiceInteractionUpdateName: TestThingsServiceInteraction {
    
    override func updateAssociatedThing(physicalID: String, firmware: String?, name: String?, customSettings: [String : Any]?, thingStatus: ThingStatus?, updateThingClosure: @escaping UpdateThingClosure) {
        XCTAssertEqual(physicalID, FacadeThingTests.testThing.thingInfo.physicalId)
        XCTAssertNil(firmware)
        XCTAssertEqual(name, FacadeThingTests.updatedName)
        XCTAssertNil(thingStatus)
        XCTAssertNil(customSettings)
        let res: Response<Thing, UpdateThingError> = .success(FacadeThingTests.testThing)
        updateThingClosure(res)
    }
}
