//
//  MessagesStructureTests.swift
//  CopilotAPIAccessTests
//
//  Created by Elad on 24/03/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class MessagesStructureTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testWrongActionParameter() {
//        let wrongCtaDictionary = loadJsonFileWith("get-in-app-messages-new-action")
//        XCTAssertNil(InAppMessages(withDictionary: wrongCtaDictionary))
    }
    
    //MARK: - Helper methods
    
    private func loadJsonFileWith(_ name: String) -> Dictionary<String, Any> {
        guard let pathString = Bundle(for: type(of: self)).path(forResource: name, ofType: "json") else {
            fatalError("UnitTestData.json not found")
        }

        guard let jsonString = try? String(contentsOfFile: pathString, encoding: .utf8) else {
            fatalError("Unable to convert UnitTestData.json to String")
        }

        print("The JSON string is: \(jsonString)")

        guard let jsonData = jsonString.data(using: .utf8) else {
            fatalError("Unable to convert UnitTestData.json to Data")
        }

        guard let jsonDictionary = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [String:Any] else {
            fatalError("Unable to convert UnitTestData.json to JSON dictionary")
        }

        print("The JSON dictionary is: \(jsonDictionary)")
        return jsonDictionary
    }

}
