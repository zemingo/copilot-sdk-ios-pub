//
//  MockConfigurationProvider.swift
//  CopilotAPIAccessTests
//
//  Created by Elad on 02/01/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation
@testable import CopilotAPIAccess

class MockConfigurationProvider: ConfigurationProvider {
    
    var baseUrl: String? = "some mock baseUrl"
    
    var applicationId: String? = "some mock applicationId"
    
    var isGdprCompliant: Bool? = false
    
    var manageType: ManageType = .yourOwn
    
    var appVersion: String = "some mock appVersion"
    
}
