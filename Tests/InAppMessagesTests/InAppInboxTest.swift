//
//  InAppInboxTest.swift
//  CopilotAPIAccessTests
//
//  Created by Revital Pisman on 24/12/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class InAppInboxTest: XCTestCase {
    
    private var inbox: InAppInbox? = nil
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        inbox = nil
        
    }
    
    
    //MARK: - test populateMessages

    
//    func testPopulateMessagesInsertionOfNewMessageDifferentId() {
//
//        guard let inbox = inbox else { return }
//        var messages = [InAppMessage]()
//
//        let currentDate = Date()
//        let iamSomeMessage1 = InAppMessage(id: UUID().uuidString,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "mock_trigger"),
//                                           priority: .high,
//                                           status: .ready,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 2)!)
//        messages.append(iamSomeMessage1)
//        inbox.populateMessages(messages)
//        XCTAssertEqual(inbox.messages.count, 1)
//
//        var messages2 = [InAppMessage]()
//        let iamSomeMessage2 = InAppMessage(id: UUID().uuidString,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "mock_trigger"),
//                                           priority: .low,
//                                           status: .ready,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 2)!)
//
//        messages2.append(iamSomeMessage2)
//
//        inbox.populateMessages(messages2)
//
//        XCTAssertEqual(inbox.messages.count, 2)
//        XCTAssert(inbox.messages.contains(where: { $0.id == messages[0].id }) )
//        XCTAssert(inbox.messages.contains(where: { $0.id == messages2[0].id }) )
//    }
    
//    func testPopulateMessagesInsertionOfNewMessagesToExistingMessagesWithDifferentIds() {
//
//        guard let inbox = inbox else { return }
//        var messages = [InAppMessage]()
//
//        let currentDate = Date()
//        let iamSomeMessage1 = InAppMessage(id: UUID().uuidString,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "mock_trigger"),
//                                           priority: .high,
//                                           status: .ready,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 2)!)
//        let iamSomeMessage2 = InAppMessage(id: UUID().uuidString,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "mock_trigger"),
//                                           priority: .low,
//                                           status: .ready,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 2)!)
//
//        messages.append(iamSomeMessage1)
//        messages.append(iamSomeMessage2)
//        inbox.populateMessages(messages)
//        XCTAssertEqual(inbox.messages.count, 2)
//
//        var messages2 = [InAppMessage]()
//        let iamSomeMessage3 = InAppMessage(id: UUID().uuidString,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "mock_trigger"),
//                                           priority: .low,
//                                           status: .ready,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 2)!)
//        let iamSomeMessage4 = InAppMessage(id: UUID().uuidString,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "mock_trigger"),
//                                           priority: .low,
//                                           status: .ready,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 2)!)
//
//        messages2.append(iamSomeMessage3)
//        messages2.append(iamSomeMessage4)
//        inbox.populateMessages(messages2)
//
//        XCTAssertEqual(inbox.messages.count, 4)
//        XCTAssert(inbox.messages.contains(where: { $0.id == messages[0].id }) )
//        XCTAssert(inbox.messages.contains(where: { $0.id == messages[1].id }) )
//        XCTAssert(inbox.messages.contains(where: { $0.id == messages2[0].id }) )
//        XCTAssert(inbox.messages.contains(where: { $0.id == messages2[1].id }) )
//    }
    
//    func testPopulateMessagesInsertionOfNewMessageSameIdWithStatusNotEvicted() {
//
//        guard let inbox = inbox else { return }
//        var messages = [InAppMessage]()
//
//        let currentDate = Date()
//        let someUuid = UUID().uuidString
//        let someUuid2 = UUID().uuidString
//        let iamSomeMessage1 = InAppMessage(id: someUuid,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "mock_trigger"),
//                                           priority: .high,
//                                           status: .ready,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 2)!)
//        let iamSomeMessage2 = InAppMessage(id: someUuid2,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "mock_trigger"),
//                                           priority: .low,
//                                           status: .pending,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 2)!)
//
//        messages.append(iamSomeMessage1)
//        messages.append(iamSomeMessage2)
//        inbox.populateMessages(messages)
//        XCTAssertEqual(inbox.messages.count, 2)
//
//        var messages2 = [InAppMessage]()
//        messages2.append(iamSomeMessage1)
//        messages2.append(iamSomeMessage2)
//
//        inbox.populateMessages(messages2)
//
//        XCTAssertEqual(inbox.messages.count, 2)
//        XCTAssert(inbox.messages.contains(where: { $0.id == messages[0].id }) )
//        XCTAssert(inbox.messages.contains(where: { $0.id == messages[1].id }) )
//        XCTAssert(inbox.messages.contains(where: { $0.id == messages2[0].id }) )
//        XCTAssert(inbox.messages.contains(where: { $0.id == messages2[1].id }) )
//    }
    
//    func testPopulateMessagesInsertionOfNewMessageSameIdWithStatusEvicted() {
//
//        guard let inbox = inbox else { return }
//        var messages = [InAppMessage]()
//
//        let currentDate = Date()
//        let someUuid = UUID().uuidString
//        let someUuid2 = UUID().uuidString
//        let iamSomeMessage1 = InAppMessage(id: someUuid,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "mock_trigger"),
//                                           priority: .high,
//                                           status: .ready,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 2)!)
//        let iamSomeMessage2 = InAppMessage(id: someUuid2,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "mock_trigger"),
//                                           priority: .low,
//                                           status: .pending,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 2)!)
//
//        messages.append(iamSomeMessage1)
//        messages.append(iamSomeMessage2)
//        inbox.populateMessages(messages)
//        XCTAssertEqual(inbox.messages.count, 2)
//
//        var messages2 = [InAppMessage]()
//
//        let iamSomeMessage3 = InAppMessage(id: someUuid,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "mock_trigger"),
//                                           priority: .high,
//                                           status: .evicted,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 2)!)
//        let iamSomeMessage4 = InAppMessage(id: someUuid2,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "mock_trigger"),
//                                           priority: .low,
//                                           status: .evicted,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 2)!)
//        messages2.append(iamSomeMessage3)
//        messages2.append(iamSomeMessage4)
//
//        inbox.populateMessages(messages2)
//
//        XCTAssertEqual(inbox.messages.count, 2)
//
//        inbox.messages.forEach { (message) in
//            XCTAssert(message.status == .evicted)
//        }
//    }
    
//    func testPopulateMessagesInsertALotOfMessagesWithDifferentIdsDifferentQueue() {
//
//        guard let inbox = inbox else { return }
//
//        let testCount = 50
//
//        let expectation = self.expectation(description: #function)
//        expectation.expectedFulfillmentCount = testCount
//
//        for index in 0 ..< testCount {
//            DispatchQueue.global().async {
//                let currentDate = Date()
//                let randomUuid = UUID().uuidString
//                let iamSomeMessage = InAppMessage(id: randomUuid,
//                                                  model: self.simpleIamModel,
//                                                  trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                                  priority: IamPriority.high,
//                                                  status: InAppStatus.pending,
//                                                  creationDate: currentDate.add(seconds: index)!,
//                                                  expirationDate: currentDate.add(days: 3)!)
//
//                inbox.populateMessages([iamSomeMessage])
//                expectation.fulfill()
//            }
//        }
//        wait(for: [expectation], timeout: 10)
//        XCTAssertEqual(inbox.messages.count, testCount)
//    }
    
    //MARK: - test markMessageAsEvicted
    
//    func testMarkMessageAsEvicted() {
//
//        guard let inbox = inbox else { return }
//
//        var messages = [InAppMessage]()
//
//        let currentDate = Date()
//        let message1 = InAppMessage(id: UUID().uuidString,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                           priority: IamPriority.high,
//                                           status: InAppStatus.pending,
//                                           creationDate: currentDate,
//                                           expirationDate: currentDate.add(days: 3)!)
//
//        let message2 = InAppMessage(id: UUID().uuidString,
//                                          model: simpleIamModel,
//                                          trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                          priority: IamPriority.high,
//                                          status: InAppStatus.pending,
//                                          creationDate: currentDate,
//                                          expirationDate: currentDate.add(days: 3)!)
//
//        messages.append(message1)
//        messages.append(message2)
//
//        inbox.populateMessages(messages)
//
//        inbox.markMessageAsEvicted(message2)
//
//        for message in inbox.messages {
//            if message.id == message1.id {
//                //status should be pending
//                XCTAssertEqual(message.status, InAppStatus.pending)
//            } else {
//                //status should be evicted
//                XCTAssertEqual(message.status, InAppStatus.evicted)
//            }
//        }
//    }
    
    //MARK: - test readyToTrigger
    
//    func testEventTriggeredSimpleTriggerWithOneElementWithStatusReady() {
//        guard let inbox = inbox else { return }
//
//        var messages = [InAppMessage]()
//
//        let currentDate = Date()
//        let iamStatusReady = InAppMessage(id: UUID().uuidString,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                           priority: IamPriority.high,
//                                           status: InAppStatus.ready,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 3)!)
//        messages.append(iamStatusReady)
//        inbox.populateMessages(messages)
//
//        let testTrigger = CheckTrigger() { (message, wasCalled) in
//            XCTAssertNotNil(message)
//            guard let message = message else {
//                XCTFail("this test should return one message with status of ready")
//                return
//            }
//            XCTAssertEqual(message.status, iamStatusReady.status)
//            XCTAssertTrue(wasCalled)
//        }
//        inbox.delegate = testTrigger
//        inbox.readyToTrigger()
//
//        let testTrigger2 = CheckTrigger()
//        inbox.delegate = testTrigger2
//
//        var messages2 = [InAppMessage]()
//        let iamStatusReady2 = InAppMessage(id: UUID().uuidString,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                           priority: IamPriority.high,
//                                           status: InAppStatus.ready,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 3)!)
//        messages2.append(iamStatusReady2)
//        inbox.populateMessages(messages2)
//
//        inbox.readyToTrigger()
//        XCTAssertTrue(!testTrigger.wasCalled)
//
//
//
//    }
    
//    func testEventTriggeredSimpleTriggerWithMultipleElementsWithStatusReady() {
//        guard let inbox = inbox else { return }
//
//        var messages = [InAppMessage]()
//
//        let currentDate = Date()
//        let iamStatusReady = InAppMessage(id: UUID().uuidString,
//                                          model: simpleIamModel,
//                                          trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                          priority: IamPriority.low,
//                                          status: InAppStatus.ready,
//                                          creationDate: currentDate.subtract(days: 2),
//                                          expirationDate: currentDate.add(days: 3)!)
//
//        let iamStatusReady2 = InAppMessage(id: UUID().uuidString,
//                                          model: simpleIamModel,
//                                          trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                          priority: IamPriority.medium,
//                                          status: InAppStatus.ready,
//                                          creationDate: currentDate.subtract(days: 2),
//                                          expirationDate: currentDate.add(days: 3)!)
//
//        let iamStatusReady3 = InAppMessage(id: UUID().uuidString,
//                                          model: simpleIamModel,
//                                          trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                          priority: IamPriority.low,
//                                          status: InAppStatus.ready,
//                                          creationDate: currentDate.subtract(days: 2),
//                                          expirationDate: currentDate.add(days: 3)!)
//
//        let iamStatusReady4 = InAppMessage(id: UUID().uuidString,
//                                          model: simpleIamModel,
//                                          trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                          priority: IamPriority.high,
//                                          status: InAppStatus.ready,
//                                          creationDate: currentDate.subtract(days: 2),
//                                          expirationDate: currentDate.add(days: 3)!)
//
//        messages.append(iamStatusReady)
//        messages.append(iamStatusReady2)
//        messages.append(iamStatusReady3)
//        messages.append(iamStatusReady4)
//
//        inbox.populateMessages(messages)
//
//        let testTrigger = CheckTrigger() { (message, wasCalled) in
//            XCTAssertNotNil(message)
//            guard let message = message else {
//                XCTFail("this test should contains messages with status ready")
//                return
//            }
//            XCTAssertEqual(message.priority, iamStatusReady4.priority)//priority high
//            XCTAssertEqual(message.status, iamStatusReady4.status)
//            XCTAssertTrue(wasCalled)
//        }
//        inbox.delegate = testTrigger
//        inbox.readyToTrigger()
//
//        let testTrigger2 = CheckTrigger()
//        inbox.delegate = testTrigger2
//
//        var messages2 = [InAppMessage]()
//        let iamStatusReady5 = InAppMessage(id: UUID().uuidString,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                           priority: IamPriority.high,
//                                           status: InAppStatus.ready,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 3)!)
//        messages2.append(iamStatusReady5)
//        inbox.populateMessages(messages2)
//
//        inbox.readyToTrigger()
//        XCTAssertTrue(!testTrigger.wasCalled)
//
//
//
//    }
    
//    func testEventTriggeredSimpleTriggerWithOneElementWithStatusNotReady() {
//        guard let inbox = inbox else { return }
//
//        var messages = [InAppMessage]()
//
//        let currentDate = Date()
//        let iamStatusPending = InAppMessage(id: UUID().uuidString,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                           priority: IamPriority.high,
//                                           status: InAppStatus.pending,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 3)!)
//        messages.append(iamStatusPending)
//        inbox.populateMessages(messages)
//
//        let testTrigger = CheckTrigger() { (message, wasCalled) in
//
//            XCTFail("this test should no get in here")
//        }
//        inbox.delegate = testTrigger
//        inbox.readyToTrigger()
//
//        XCTAssertTrue(!testTrigger.wasCalled)
//    }
    
    //MARK: - test analyticsEventReported
    
//    func testAnalyticsEventReportedWithNoMessagesInInbox() {
//
//        let inbox: InAppInbox = InAppInbox(touchPolicyValidator: DefaultTouchPolicyValidator())
//
//        let testTrigger = CheckTrigger() { (message, wasCalled) in
//            XCTFail("should not get in here because there is no messages in iamInbox")
//        }
//        inbox.delegate = testTrigger
//
//        provideAnalyticsEventDispatcher().dispatcherLogCustomEvent(event: MockAnalyticsEvent())
//
//        XCTAssertTrue(!testTrigger.wasCalled)
//    }
    
//    func testAnalyticsEventReportedWithMessageInInbox() {
//
//        let inbox: InAppInbox = InAppInbox(touchPolicyValidator: DefaultTouchPolicyValidator())
//
//        let testTrigger = CheckTrigger() { (message, wasCalled) in
//            XCTAssertNotNil(message)
//            guard let _ = message else {
//                XCTFail("this test should contain messages that not marked as evicted")
//                return
//            }
//            XCTAssertTrue(wasCalled)
//        }
//        inbox.delegate = testTrigger
//
//        var messages = [InAppMessage]()
//
//        let currentDate = Date()
//        let iamStatusPending = InAppMessage(id: UUID().uuidString,
//                                           model: simpleIamModel,
//                                           trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                           priority: IamPriority.high,
//                                           status: InAppStatus.pending,
//                                           creationDate: currentDate.subtract(days: 2),
//                                           expirationDate: currentDate.add(days: 3)!)
//        messages.append(iamStatusPending)
//
//        inbox.populateMessages(messages)
//
//        provideAnalyticsEventDispatcher().dispatcherLogCustomEvent(event: MockAnalyticsEvent())
//    }
    
//    func testAnalyticsEventReportedWithEvictedMessageInInbox() {
//
//        let inbox: InAppInbox = InAppInbox(touchPolicyValidator: DefaultTouchPolicyValidator())
//
//        let testTrigger = CheckTrigger() { (message, wasCalled) in
//            XCTFail("should not get in here because the message status is evicted")
//        }
//
//        inbox.delegate = testTrigger
//
//
//        var messages = [InAppMessage]()
//
//        let currentDate = Date()
//        let iamStatusPending = InAppMessage(id: UUID().uuidString,
//                                            model: simpleIamModel,
//                                            trigger: SingleEventTrigger(triggerEventName: "onboarding_started"),
//                                            priority: IamPriority.high,
//                                            status: InAppStatus.evicted,
//                                            creationDate: currentDate.subtract(days: 2),
//                                            expirationDate: currentDate.add(days: 3)!)
//        messages.append(iamStatusPending)
//
//        inbox.populateMessages(messages)
//
//        provideAnalyticsEventDispatcher().dispatcherLogCustomEvent(event: MockAnalyticsEvent())
//
//        XCTAssertTrue(!testTrigger.wasCalled)
//    }
}

//MARK: - helpers

//class CheckTrigger: MessageTriggeredDelegate {
//
//    var returnClosure: ((InAppMessage?, Bool) -> ())?
//    var wasCalled:Bool
//
//    init(returnClosure:((InAppMessage?, Bool) -> ())? = nil) {
//        self.wasCalled = false
//        self.returnClosure = returnClosure
//    }
//
//    func messageTriggeered(message: InAppMessage) {
//        wasCalled = true
//        returnClosure?(message, wasCalled)
//    }
//}

//MARK: - provide AnalyticsEventDispatcher
//extension InAppInboxTest {
//    func provideAnalyticsEventDispatcher() -> AnalyticsEventsDispatcher {
//        let copilotConfigurationProvider = MockConfigurationProvider()
//        let sessionBasedAnalyticsRepository: SessionBasedAnalyticsRepository = SessionBasedAnalyticsRepository()
//        return AnalyticsEventsDispatcher(eventsProviderHandler: AnalyticsEventsProviderHandler(copilotConfigurationProvider: copilotConfigurationProvider), generalParametersRepository: sessionBasedAnalyticsRepository)
//    }
//}


