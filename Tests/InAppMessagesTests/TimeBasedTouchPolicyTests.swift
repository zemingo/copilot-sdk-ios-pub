//
//  TimeBasedTouchPolicyTests.swift
//  CopilotAPIAccessTests
//
//  Created by Elad on 06/01/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class TimeBasedTouchPolicyTests: XCTestCase {

    //dictionary example
//    ▿ 0 : 2 elements
//      - key : "_type"
//      - value : TimeBased
//    ▿ 1 : 2 elements
//      - key : "minSecondsBetweenInteractions"
//      - value : 600

    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testCanInteractWithUserBeforeMinTimeFromPreviousInteractionPassed() {
        let timeBasedTouchPolicyDictionary: [String : Any] = ["minSecondsBetweenInteractions" : 10 as TimeInterval]
        let timeBasedTouchPolicy = TimeBasedTouchPolicyValidator(withDictionary: timeBasedTouchPolicyDictionary)!

        timeBasedTouchPolicy.setInteracted()
        sleep(4)
        XCTAssertFalse(timeBasedTouchPolicy.canInteractWithUser())
        
        sleep(1)
        
        XCTAssertFalse(timeBasedTouchPolicy.canInteractWithUser())
        
        sleep(6)
        
        XCTAssertTrue(timeBasedTouchPolicy.canInteractWithUser())
    }
    
    func testCanInteractWithUserAfterMinTimeFromPreviousInteractionPassed() {
        let timeBasedTouchPolicyDictionary: [String : Any] = ["minSecondsBetweenInteractions" : 5 as TimeInterval]
        let timeBasedPolicy = TimeBasedTouchPolicyValidator(withDictionary: timeBasedTouchPolicyDictionary)!
        timeBasedPolicy.setInteracted()
        
        sleep(6)
        XCTAssertEqual(timeBasedPolicy.canInteractWithUser(), true)
    }
}
