//
//  UserResolversTest.swift
//  CopilotAPIAccessTests
//
//  Created by Adaya on 05/03/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import XCTest
@testable import CopilotAPIAccess

class FetchMeErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = FetchMeError
    typealias R = FetchMeErrorResolver
    
    let msg = "msg"
    
    func resolver() -> FetchMeErrorResolver {
        return FetchMeErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .generalError(_):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .requiresRelogin(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    
    func test_Unrecognized(){
        assertUnrecognized()
    }
}



class UpdateUserDetailsResolverTests: XCTestCase, ResolverTest {
    
    typealias T = UpdateUserDetailsError
    typealias R = UpdateUserDetailsErrorResolver
    
    let msg = "msg"
    
    func resolver() -> UpdateUserDetailsErrorResolver {
        return UpdateUserDetailsErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .invalidParameters(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .requiresRelogin(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
   
    
    func test_Unrecognized(){
        assertUnrecognized()
    }
    
}

class UpdateDeviceDetailsResolverTests: XCTestCase, ResolverTest {
    
    typealias T = UpdateDeviceDetailsError
    typealias R = UpdateDeviceDetailsErrorResolver
    
    let msg = "msg"
    
    func resolver() -> UpdateDeviceDetailsErrorResolver {
        return UpdateDeviceDetailsErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .invalidParameters(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .requiresRelogin(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    
    func test_Unrecognized(){
        assertUnrecognized()
    }
    
}
