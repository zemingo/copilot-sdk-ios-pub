//
//  MockSessionObserver.swift
//  CopilotAPIAccess
//
//  Created by Elad on 19/01/2020.
//  Copyright © 2020 Zemingo. All rights reserved.
//

import Foundation

class MockSessionObserver: SessionLifeTimeObserver {
    func sessionStarted() {
        print("Logged in session started")
    }
    
    func sessionEnded() {
        print("Logged out session ended")
    }

}
