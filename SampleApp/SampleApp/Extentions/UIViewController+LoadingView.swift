//
//  UIViewController+LoadingView.swift
//  SampleApp
//
//  Created by Revital Pisman on 15/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showLoadingView(animated: Bool = true, completion: (() -> ())? = nil) {
        let vc = LoadingViewController()
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        DispatchQueue.main.async { [weak self] in
            self?.present(vc, animated: animated, completion: completion)
        }
    }
    
    func hideLoadingView(animated: Bool = true, completion: (() -> ())? = nil) {
        DispatchQueue.main.async { [weak self] in
            if let loadingVC = self?.presentedViewController as? LoadingViewController {
                if !loadingVC.isBeingDismissed {
                    loadingVC.dismiss(animated: animated, completion: completion)
                }
                else {
                    completion?()
                }
            }
            else {
                completion?()
            }
        }
    }
    
}
