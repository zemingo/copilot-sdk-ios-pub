//
//  Bundle+Configuration.swift
//  SampleApp
//
//  Created by Revital Pisman on 10/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

enum ManageType: String {
    case YourOwn
    case CopilotConnect
}

extension Bundle {
    
    private struct Consts {
        static let plistType = "plist"
        static let copilotPlistName = "Copilot-Info"
        static let googleServicePlistName = "GoogleService-Info"
        static let authenticationParams = "Authentication Params"
        static let environmentUrl = "ENVIRONMENT_URL"
        static let applicationId = "APPLICATION_ID"
        static let isGDPRCompliant = "IS_GDPR_COMPLIANT"
        static let firebaseProjectID = "PROJECT_ID"
        static let firebaseDatabaseUrl = "DATABASE_URL"
        static let manageType = "MANAGE_TYPE"
    }
    
    private var copilotPlistFile: String {
        guard let plistPath = path(forResource: Consts.copilotPlistName, ofType: Consts.plistType) else {
            fatalError("Copilot-Info.plist file not found.")
        }
        return plistPath
    }
    
    private var googleServicePlistFile: String {
        guard let plistPath = path(forResource: Consts.googleServicePlistName, ofType: Consts.plistType) else {
            fatalError("Copilot-Info.plist file not found.")
        }
        return plistPath
    }
    
    private var copilotConfiguration: [String : Any]? {
        return NSDictionary(contentsOfFile: copilotPlistFile) as? [String: Any]
    }
    
    private var googleServiceConfiguration: [String : Any]? {
        return NSDictionary(contentsOfFile: googleServicePlistFile) as? [String: Any]
    }
    
    private var authenticationParams: [String : Any]? {
        return copilotConfiguration?[Consts.authenticationParams] as? [String : Any]
    }
    
    var copilotEnvironmentUrl: String? {
        return authenticationParams?[Consts.environmentUrl] as? String
    }
    
    var copilotApplicationId: String? {
        return authenticationParams?[Consts.applicationId] as? String
    }
    
    var isGDPRCompliant: Bool? {
        return copilotConfiguration?[Consts.isGDPRCompliant] as? Bool
    }
    
    var copilotManageType: String? {
        return copilotConfiguration?[Consts.manageType] as? String
    }
    
    var firebaseProjectID: String? {
        return googleServiceConfiguration?[Consts.firebaseProjectID] as? String
    }
    
    var firebaseDatabaseUrl: String? {
        return googleServiceConfiguration?[Consts.firebaseDatabaseUrl] as? String
    }
    
    var manageType: ManageType {
        guard let copilotManageType = copilotManageType else {
            fatalError("Copilot-Info.plist file is missing the mandatory `MANAGE_TYPE` key, it should point to a String value.")
        }
        
        if copilotManageType.lowercased() == ManageType.YourOwn.rawValue.lowercased() {
            return .YourOwn
        } else if copilotManageType.lowercased() == ManageType.CopilotConnect.rawValue.lowercased() {
            return .CopilotConnect
        } else {
            fatalError("Copilot-Info.plist file is missing the mandatory `MANAGE_TYPE` key, it should point to a String value `YourOwn` or `CopilotConnect`.")
        }
    }
    
}
