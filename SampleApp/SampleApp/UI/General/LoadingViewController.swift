//
//  LoadingViewController.swift
//  SampleApp
//
//  Created by Revital Pisman on 15/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {
    
    // MARK: - Properties
    
    private let darkOverlayView = UIView()
    private let activityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
    
    //Consts
    private let loadingViewBackgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.3)
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        activityIndicatorView.center = view.center
        activityIndicatorView.startAnimating()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        activityIndicatorView.stopAnimating()
    }
    
    
    // MARK: - Private Functions
    
    private func setupUI() {
        self.view.backgroundColor = loadingViewBackgroundColor
        
        darkOverlayView.frame = self.view.bounds
        darkOverlayView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        self.view.addSubview(darkOverlayView)
        self.view.addSubview(activityIndicatorView)
        
        activityIndicatorView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        activityIndicatorView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
}

