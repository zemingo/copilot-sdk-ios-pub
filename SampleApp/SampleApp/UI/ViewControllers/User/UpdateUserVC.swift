//
//  UpdateUserVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 14/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class UpdateUserVC: UIViewController {

    //MARK: - Properties
    
    @IBOutlet weak var tableView: UITableView!
    
    private var datasource = UpdateUserCommands.allCases
    
    private enum UpdateUserCommands: Int, CaseIterable {
        case updateUserDetails
        case approveTermsOfUse
        case updateUsersConsent
        case changePassword
        
        var title: String {
            switch self {
            case .updateUserDetails:
                return "Update user details"
            case .approveTermsOfUse:
                return "Approve terms of use"
            case .updateUsersConsent:
                return "Update user's consent"
            case .changePassword:
                return "Change password"
            }
        }
        
        var vc: UIViewController? {
            switch self {
            case .updateUserDetails:
                return ViewControllersFactory.createControllerWithType(.updateUserDetails)
            case .approveTermsOfUse:
                return ViewControllersFactory.createControllerWithType(.approveTermsOfUse)
            case .updateUsersConsent:
                return ViewControllersFactory.createControllerWithType(.updateUsersConsent)
            case .changePassword:
                return ViewControllersFactory.createControllerWithType(.changePassword)
            }
        }
    }
    
    //MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Update user"
    }

}

extension UpdateUserVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UpdateUserTableViewCell", for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = datasource[indexPath.row].title
        
        return cell
    }
    
}

extension UpdateUserVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let command = datasource[indexPath.row]
        
        if let vc = command.vc {
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            //Nothing happens
            print("Failed create VC for command: \(command)")
        }
    }
    
}

