//
//  RafVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 30/07/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess

class RafVC: KeyboardHandlerVC {
    
    //MARK: - Properties
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Raf"
    }
    
    
    @IBAction func getRafDataButtonPressed(_ sender: Any) {
        Copilot.instance
            .referAFriend
            .fetchRafData()
            .build()
            .execute { [weak self] (response) in
                
                DispatchQueue.main.async(execute:
                    {
                        self?.hideLoadingView() { [weak self] in
                            switch response {
                            case .success(let rafData):
                                self?.showAlert("Fetch user details succeeded")
                                
                                
                                
                            case .failure(let fetchMeError):
                                let msg: String
                                
                                switch fetchMeError {
                                case .requiresRelogin(let debugMessage):
                                    msg = "Requires Relogin: \(debugMessage)"
                                case .connectivityError(let debugMessage):
                                    msg = "Connectivity: \(debugMessage)"
                                case .generalError(let debugMessage):
                                    msg = "General: \(debugMessage)"
                                }
                                
                                self?.showAlert("Fetch user details failed with error: \(msg)\n\n\(fetchMeError.localizedDescription)")
                            }
                        }
                })
        }
    }
    
    
}
