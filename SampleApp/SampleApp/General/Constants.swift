//
//  Constants.swift
//  SampleApp
//
//  Created by Revital Pisman on 16/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct CustomValue<T: Encodable> {
    let key: String
    let value: T
}

struct Constants {
    
    static let myCustomObject1 = MyCustomObject.init(myCustomString: "String1", myCustomInt: 123, myCustomBoolean: true)
    static let myCustomObject2 = MyCustomObject.init(myCustomString: "String2", myCustomInt: 321, myCustomBoolean: false)
    
    //User
    static let consentKey1Name = "CONSENT_KEY_1"
    static let consentKey2Name = "CONSENT_KEY_2"
    
    static let userBoolCustomValue = CustomValue.init(key: "MY_USER_BOOLEAN_KEY", value: true)
    static let userStringCustomValue = CustomValue.init(key: "MY_USER_STRING_KEY", value: "String!!")
    static let userIntCustomValue = CustomValue.init(key: "MY_USER_INT_KEY", value: 123)
    static let userObjectCustomValue = CustomValue.init(key: "MY_USER_OBJECT_KEY", value: Constants.myCustomObject1)
    static let userCollectionCustomValue = CustomValue.init(key: "MY_USER_COLLECTION_KEY", value: [Constants.myCustomObject1, Constants.myCustomObject2])
    
    //Thing
    static let thingBoolCustomValue = CustomValue.init(key: "MY_THING_BOOLEAN_KEY", value: true)
    static let thingStringCustomValue = CustomValue.init(key: "MY_THING_STRING_KEY", value: "String!!")
    static let thingIntCustomValue = CustomValue.init(key: "MY_THING_INT_KEY", value: 123)
    static let thingObjectCustomValue = CustomValue.init(key: "MY_THING_OBJECT_KEY", value: Constants.myCustomObject1)
    static let thingCollectionCustomValue = CustomValue.init(key: "MY_THING_COLLECTION_KEY", value: [Constants.myCustomObject1, Constants.myCustomObject2])
}
