require 'tempfile'
require 'fileutils'
require 'rubygems'
require 'io/console'

############################################
############                    ############
############       Tasks        ############
############                    ############
############################################

desc 'Release a version, take the version from the Info.plist file or enter it manually (updating Info.plist), then build each of the frameworks, compress them, upload them to a remote location, and finally take the version from the Info.plist file, commit, tag, push, update podspecs and push to specs repo.'
task :release do |task, args|  
  
  version = determine_version
  build
  files = compress_files
  upload_files(files)
  cleanup
  update_podspecs(version)
  commit_tag_and_push(version)
  push_specs
end

task :push_podspecs do |task, args|
  push_specs
end

task :build do |task, args|
  build
end

task :compress_files do |task, args|
  compress_files
end

task :upload_files do |task, args|
  files = args[:files]
  upload_files(files)
end

############################################
############                    ############
############       Helpers      ############
############                    ############
############################################

def determine_version
  version = get_version_from_plist
  puts ""
  puts "1. Use version #{version}"
  puts "2. Enter version Manually"
  puts "3. Abort"
  input = ""
  while !['1','2','3'].include?(input)
    STDOUT.puts "Action (1-3): "
    input = STDIN.gets.strip
  end

  if input == '2'
    plist_version = version
    STDOUT.puts "Enter version: "
    version = STDIN.gets.strip
    abort "Illegal version (#{version})." if version.nil? || version.scan(/\d+\.\d+\.\d+(-\w+\.\d+)?/).length == 0
    
    updated_successfully = update_plist_version(plist_version, version)
    if !updated_successfully
      abort "Aborting - Info.plist file was not updated as requested."
    end
  elsif input == '3'
    abort      
  end

  return version
end

############    Project targets build    ############

PRODUCTS_DIR = "build/Build/Products/"

desc 'This method will iterate over all targets of the project and build them one after the other'
def build  
  targets.each do |target|
    # we only archive each target using the `iphoneos` sdk becase the `iphonesimulator` build and the FAT framework are created as part of a post-action script in the targets' schemes for the archive action.
    xcodebuild("clean build archive", target, "iphoneos")
  end
end


def create_universal_build

  iphone_build_location = build_dir('Debug', 'iphoneos')
  simulator_build_location = build_dir('Debug', 'iphonesimulator')

  targets.each do |target|
    framework_location = "#{target}.framework/"
    binary_location = "#{framework_location}#{target}"

    universal_binary_location = "#{binary_location}"
    simulator_binary_location = "#{simulator_build_location}/#{binary_location}"
    iphone_binary_location = "#{iphone_build_location}/#{binary_location}"


    # make sure the destination directory exists    
    FileUtils.mkdir_p framework_location
    FileUtils.cp(simulator_binary_location, framework_location)

    sh %{lipo -create -output "#{universal_binary_location}" "#{simulator_binary_location}" "#{iphone_binary_location}"}  
  end
end

def xcodebuild(actions, target_name, sdk)

  command = "xcodebuild -project 'Copilot.xcodeproj' -scheme '#{target_name}' -configuration 'Debug' -sdk #{sdk}"

  destination = destinations[sdk]
  unless destination.nil?
    command += " -destination #{destination}"
  end

  flags = " only_active_arch=no defines_module=yes ENABLE_BITCODE=YES OTHER_CFLAGS=-fembed-bitcode BITCODE_GENERATION_MODE=bitcode"

  arch = archs[sdk]
  unless arch.nil?
    flags += " ARCHS=#{arch}"
  end

  sh command + flags + " -quiet #{actions}"

end

def targets
  return ["CopilotLogger", "ZemingoBLELayer", "CopilotAPIAccess"]
end

def destinations
  return {
    "iphonesimulator": "OS=11.4,name=iPhone X"
  }
end

def archs
  return {
    "iphonesimulator": "i386 x86_64"
  }
end

def build_dir(configuration, sdk)
  return "#{PRODUCTS_DIR}#{configuration}-#{sdk}"
end


############    Compression    ############

desc 'This method looks for the frameworks files generated, compresses them into the project folder and returns the compressed files names array'
def compress_files
  puts "Looking for framework files"
  compressed_files = Array.new  
  framework_files = get_framework_files_paths
  framework_files.each do |file_name|
      puts ""
      puts "Looking for #{file_name}"
      puts ""
      if File.directory?(file_name) 
        puts ""
        puts "Found #{file_name}, renaming it to include the version number"
        puts ""
        f_name = file_name.split(".").first
        extension = file_name.split(".").last
        version = get_version_from_plist
        formatted_version = version.gsub(".", "_")
        
        compressed_file_name = compress(f_name, formatted_version,".#{extension}")
        compressed_files.push(compressed_file_name)        
      end
  end
  formatted_version = get_version_from_plist.gsub(".", "_")
  copilot_compressed_file_name = "Copilot_#{formatted_version}.tar.gz"
  sh "tar -czvf #{copilot_compressed_file_name} LICENSE README.md"
  compressed_files.push(copilot_compressed_file_name)
  return compressed_files
end

desc 'This method compresses a file'
def compress(file_name, version, extension)
  complete_file_name = "#{file_name}_#{version}"
  compressed_file_name = "#{complete_file_name}.tar.gz"
  puts ""
  puts "🗜 🗜 🗜 🗜 Starting to compress framework file #{file_name} 🗜 🗜 🗜 🗜"
  sh "tar -czvf #{compressed_file_name} LICENSE #{file_name}#{extension}"
  puts ""  
  return compressed_file_name
end


############    Upload    ############

desc 'Upload the received files Bitbucket, authentication required'
def upload_files(files)
  #Requesting username and password to authenticate with Bitbucket    
    puts ""
    puts "🔑 🔑 🔑 🔑 Enter your bitbucket credentials 🔑 🔑 🔑 🔑"
    puts ""
    puts "🔑 🔑 🔑 🔑 Enter your username: 🔑 🔑 🔑 🔑"
    login  = STDIN.gets.strip
    puts ""
    puts "🔑 🔑 🔑 🔑 Enter your password: 🔑 🔑 🔑 🔑"
    password = STDIN.noecho(&:gets).chomp
    #Uploading the files one by one
    files.each do |compressed_file|       
      upload_file_to_bitbucket_downloads(compressed_file, login, password)
      puts ""
      puts "🗑 🗑 🗑 🗑 Finished to upload #{compressed_file}, can process to its deletion 🗑 🗑 🗑 🗑"
      FileUtils.rm_f(compressed_file)
      puts ""
      puts "🗑 🗑 🗑 🗑 Compressed file deleted 🗑 🗑 🗑 🗑"
    end
end

desc 'This method uploads the compressed file to Bitbucket repo downloads'
def upload_file_to_bitbucket_downloads(file_name, login, password)
  bitbucket_repo_owner = get_repository_owner
  bitbucket_repo_slug = get_repository_name
  current_folder = Dir.pwd
  file_full_path = "#{current_folder}/#{file_name}"
      puts ""
      puts "🚀 🚀 🚀 🚀 Uploading file: #{file_full_path} 🚀 🚀 🚀 🚀"      
      f_name = file_name.split(".").first
      sh "curl POST --user #{login}:#{password} \"https://api.bitbucket.org/2.0/repositories/#{bitbucket_repo_owner}/#{bitbucket_repo_slug}/downloads\" --form files=@\"#{file_full_path}\""
end

def cleanup
  puts ""
  puts "Removing build directory"
  get_framework_files_paths.each do |framework| 
    FileUtils.remove_dir(framework)
  end
end

desc 'This method returns the name of the owner of the bitbucket repository'
def get_repository_owner
  return 'zemingo'
end

desc 'This method return the name of the repository name'
def get_repository_name
  return 'copilot-sdk-ios-releases'  
end

desc 'This method returns an array of paths for the required frameworks'
def get_framework_files_paths
  return targets.map { |t| "#{t}.framework" }
end

############       Git          ############

def commit_tag_and_push(version)
  puts "Committing, tagging, and pushing."
  message = "Releasing version #{version}."
  sh "git commit -am '#{message}'"
  sh "git tag #{version} -m '#{message}'"
  sh "git push --follow-tags"
end

############       Plist        ############

def update_podspecs(version)
  puts "Updating podspec files."
  podspec_files = get_podspecs
  podspec_files.each do |filename|
    framework_name = filename.split(".").first
    formatted_version = version.gsub(".", "_")    
    contents = File.read(filename)
    contents.gsub!(/s\.version\s*=\s"\d+\.\d+\.\d+(-\w+\.\d)?"/, "s.version      = \"#{version}\"")
    contents.gsub!(/s\.source\s*=\s{\s:http\s=>\s"https:\/\/\S*"\s}/, "s.source       = { :http => \"https://bitbucket.org/#{get_repository_owner}/#{get_repository_name}/downloads/#{framework_name}_#{formatted_version}.tar.gz\" }")
    File.open(filename, 'w') { |file| file.puts contents }
  end   
end

desc 'This method looks for the specific version the Info.plist file returns the stripped version string.'
def get_version_from_plist
  puts "Getting the version from the plist."
  filepath = plist_file_path
  contents = File.open(filepath).to_a
  version = nil
  contents.each_with_index do |line, index|
    if is_plist_version_key_line(line)
      version_line = contents[index + 1]
      version_line.strip!
      version_line.slice!("<string>")
      version = version_line.chomp("</string>")
      puts "Found version #{version}."
      break
    end
  end
  return version
end

desc 'This method looks for the specific version line in the Info.plist file and replaces the existing version with a new one.'
def update_plist_version(old_version, new_version)
  if old_version == new_version
    puts "Illegal versions - new version is the same as the old version"
    return false
  end
  puts "Attempting to update plist file with new version"
  did_update = false
  filepath = plist_file_path
  temp_file = Tempfile.new('Info.plist') # we right the Info.plist file content into a new file, line by line, replacing only the version line
  begin
    contents = File.open(filepath).to_a
    version = nil
    version_line_index = nil
    contents.each_with_index do |line, index|
      next if index == version_line_index # the version line is written once its found so we go to the next line
      temp_line = line
      if is_plist_version_key_line(line)
        temp_file.puts line
        version_line_index = index + 1
        version_line = contents[index + 1]        
        new_line = version_line.gsub(/#{old_version}/, "#{new_version}")
        temp_file.puts new_line
        if new_line == version_line
          puts "Couldn't find version #{old_version} in the Info.plist file, no update made."
          return false
        else
          puts "Successfully updated Info.plist version from #{old_version} to #{new_version}"
          did_update = true
        end
      else
        temp_file.puts line
      end      
    end
    temp_file.close
    FileUtils.mv(temp_file.path, filepath) # when finished, move the temporary file we created to the Info.plist file location    
  ensure
    temp_file.close
    temp_file.unlink    
  end
  return did_update
end

def is_plist_version_key_line(line)
  return line.include?('CFBundleShortVersionString')  
end

def plist_file_path
  return "Sources/Supporting\ Files/Info.plist"
end


############       Podspecs      ############

def push_specs
  podspec_files = get_podspecs
  repo_name = get_cocoapods_repo_name
  puts "Pushing the podspec files to the specs repo."
  podspec_files.each do |filename|    
    sh "pod repo push #{repo_name} #{filename} --allow-warnings --verbose"  
  end    
end

def get_podspecs
  return ["CopilotLogger.podspec", "CopilotAPIAccess.podspec", "ZemingoBLELayer.podspec", "Copilot.podspec"]
end

desc 'This method looks for all Cocoapods repos under ~/.cocoapods/repos and asks the user for the desired repo and returns it.'
def get_cocoapods_repo_name
  puts "Getting the desired cocoapods repository name."
  cocoapods_repos = Dir.entries(Dir.home + "/.cocoapods/repos")  
  if cocoapods_repos.count > 0    
    repos = []    
    cocoapods_repos.each do |repo_name|
      if !repo_name.start_with?(".")        
        repos << repo_name
      end
    end

    repo_to_use = nil
    if repos.count > 0
      if repos.count == 1
        input = ""
        while input != "y" && input != "n"    
          STDOUT.puts "Only one repo found, use `#{repos.first}`?"
          input = STDIN.gets.strip
        end
        if input == 'n'
          abort "Aborting due to unwanted cocoapods repo."
        end
        repo_to_use = repos.first
      else
        puts "Repos:"
        repos.each_with_index do |repo_name, index|
          puts "#{index + 1}. #{repo_name}"
        end
        input = 0
        while input.to_i < 1 || input.to_i > repos.count
          STDOUT.puts "Select the desired repository (1-#{repos.count}): "
          input = STDIN.gets.strip
        end
        repo_to_use = repos[input.to_i - 1]
      end
    end        
  end

  return repo_to_use
end